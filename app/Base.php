<?php

namespace Soluplastic;

use Illuminate\Database\Eloquent\Model;

class Base extends Model
{
    public function products()
    {
    	return $this->hasMany('Soluplastic\Product');
    }
}
