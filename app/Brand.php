<?php

namespace Soluplastic;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    public function products()
    {
    	return $this->hasMany('Soluplastic\Product');
    }
}
