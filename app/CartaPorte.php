<?php

namespace Soluplastic;

use Illuminate\Database\Eloquent\Model;

class CartaPorte extends Model
{
    protected $table = "product_cartaportes";
	
    public function user()
    {
    	return $this->belongsTo('Soluplastic\User');
    }
}
