<?php

namespace Soluplastic;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function products()
    {
    	return $this->hasMany('Soluplastic\Product');
    }
}
