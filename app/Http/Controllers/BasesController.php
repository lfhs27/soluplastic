<?php

namespace Soluplastic\Http\Controllers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Soluplastic\Base;

class BasesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
		$bases = Base::all();
		
        return view("dashboard.bases.index")
		->with("bases", $bases)
		->with("sidemenu", "bases");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {		
		if (Gate::allows('crud_config', null)) {
			return view("dashboard.bases.create")
			->with("sidemenu", "bases");
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
		if (Gate::allows('crud_config', null)) {
			$base = new Base;
						
			if($request->has("name"))
				$base->name = $request->name;
			else
				$base->name = "";
				
			$base->save();
			
			return redirect('/bases')->with('success', 'La base con el ID: '.$base->id.' ha sido creada.');
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
		$base = Base::find($id);
		
        return view("dashboard.bases.edit")
		->with("base", $base)
		->with("sidemenu", "bases");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
		if (Gate::allows('crud_config', null)) {
			$base = Base::find($id);
			
			if($request->has("name"))
				$base->name = $request->name;
				
			$base->save();
			
			return redirect('/bases')->with('success', 'La base con el ID: '.$base->id.' ha sido modificada.');
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
		if (Gate::allows('crud_config', null)) {
			$base = Base::find($id);
			
			if($base){
				$base->destroy($id);
				return redirect('/bases')->with('success', 'La base ha sido eliminado.');
			}else{
				return redirect('/bases')->with('error', 'La base no se pudo eliminar');
			}
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }
}
