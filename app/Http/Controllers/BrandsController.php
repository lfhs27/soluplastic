<?php

namespace Soluplastic\Http\Controllers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Soluplastic\Brand;

class BrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
		$brands = Brand::all();
		
        return view("dashboard.brands.index")
		->with("brands", $brands)
		->with("sidemenu", "marcas");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {		
		if (Gate::allows('crud_config', null)) {
			return view("dashboard.brands.create")
			->with("sidemenu", "brands");
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
		if (Gate::allows('crud_config', null)) {
			$brand = new Brand;
						
			if($request->has("name"))
				$brand->name = $request->name;
			else
				$brand->name = "";
				
			$brand->save();
			
			return redirect('/brands')->with('success', 'El brand con el ID: '.$brand->id.' ha sido creado.');
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
		$brand = Brand::find($id);
		
        return view("dashboard.brands.edit")
		->with("brand", $brand)
		->with("sidemenu", "marcas");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
		if (Gate::allows('crud_config', null)) {
			$brand = Brand::find($id);
			
			if($request->has("name"))
				$brand->name = $request->name;
				
			$brand->save();
			
			return redirect('/brands')->with('success', 'El brand con el ID: '.$brand->id.' ha sido modificado.');
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
		if (Gate::allows('crud_config', null)) {
			$brand = Brand::find($id);
			
			if($brand){
				$brand->destroy($id);
				return redirect('/brands')->with('success', 'El brand ha sido eliminado.');
			}else{
				return redirect('/brands')->with('error', 'El brand no se pudo eliminar');
			}
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }
}
