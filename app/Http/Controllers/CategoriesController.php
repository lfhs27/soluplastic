<?php

namespace Soluplastic\Http\Controllers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Soluplastic\Category;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
		$categories = Category::all();
		
        return view("dashboard.categories.index")
		->with("categories", $categories)
		->with("sidemenu", "categorias");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {		
		if (Gate::allows('crud_config', null)) {
			return view("dashboard.categories.create")
			->with("sidemenu", "categorias");
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
		if (Gate::allows('crud_config', null)) {
			$category = new Category;
						
			if($request->has("name"))
				$category->name = $request->name;
			else
				$category->name = "";
				
			$category->save();
			
			return redirect('/categories')->with('success', 'La categoría con el ID: '.$category->id.' ha sido creada.');
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
		$category = Category::find($id);
		
        return view("dashboard.categories.edit")
		->with("category", $category)
		->with("sidemenu", "categorias");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
		if (Gate::allows('crud_config', null)) {
			$category = Category::find($id);
			
			if($request->has("name"))
				$category->name = $request->name;
				
			$category->save();
			
			return redirect('/categories')->with('success', 'La categoría con el ID: '.$category->id.' ha sido modificada.');
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
		if (Gate::allows('crud_config', null)) {
			$category = Category::find($id);
			
			if($category){
				$category->destroy($id);
				return redirect('/categories')->with('success', 'La categoría ha sido eliminada.');
			}else{
				return redirect('/categories')->with('error', 'La categoría no se pudo eliminar');
			}
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }
}
