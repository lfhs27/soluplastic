<?php

namespace Soluplastic\Http\Controllers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Soluplastic\Classification;

class ClassificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
		$classifications = Classification::all();
		
        return view("dashboard.classifications.index")
		->with("classifications", $classifications)
		->with("sidemenu", "clasificaciones");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {		
		if (Gate::allows('crud_config', null)) {
			return view("dashboard.classifications.create")
			->with("sidemenu", "clasificaciones");
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
		if (Gate::allows('crud_config', null)) {
			$classification = new Classification;
						
			if($request->has("name"))
				$classification->name = $request->name;
			else
				$classification->name = "";
				
			$classification->save();
			
			return redirect('/classifications')->with('success', 'La clasificación con el ID: '.$classification->id.' ha sido creada.');
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
		$classification = Classification::find($id);
		
        return view("dashboard.classifications.edit")
		->with("classification", $classification)
		->with("sidemenu", "clasificaciones");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
		if (Gate::allows('crud_config', null)) {
			$classification = Classification::find($id);
			
			if($request->has("name"))
				$classification->name = $request->name;
				
			$classification->save();
			
			return redirect('/classifications')->with('success', 'La clasificación con el ID: '.$classification->id.' ha sido modificada.');
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
		if (Gate::allows('crud_config', null)) {
			$color = Classification::find($id);
			
			if($color){
				$color->destroy($id);
				return redirect('/classifications')->with('success', 'La clasificación ha sido eliminada.');
			}else{
				return redirect('/classifications')->with('error', 'La clasificación no se pudo eliminar');
			}
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }
}
