<?php

namespace Soluplastic\Http\Controllers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Soluplastic\Color;

class ColorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
		$colors = Color::all();
		
        return view("dashboard.colors.index")
		->with("colors", $colors)
		->with("sidemenu", "colores");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {		
		if (Gate::allows('crud_config', null)) {
			return view("dashboard.colors.create")
			->with("sidemenu", "colores");
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
		if (Gate::allows('crud_config', null)) {
			$color = new Color;
						
			if($request->has("name"))
				$color->name = $request->name;
			else
				$color->name = "";
				
			$color->save();
			
			return redirect('/colors')->with('success', 'El color con el ID: '.$color->id.' ha sido creado.');
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
		$color = Color::find($id);
		
        return view("dashboard.colors.edit")
		->with("color", $color)
		->with("sidemenu", "colores");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
		if (Gate::allows('crud_config', null)) {
			$color = Color::find($id);
			
			if($request->has("name"))
				$color->name = $request->name;
				
			$color->save();
			
			return redirect('/colors')->with('success', 'El color con el ID: '.$color->id.' ha sido modificado.');
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
		if (Gate::allows('crud_config', null)) {
			$color = Color::find($id);
			
			if($color){
				$color->destroy($id);
				return redirect('/colors')->with('success', 'El color ha sido eliminado.');
			}else{
				return redirect('/colors')->with('error', 'El color no se pudo eliminar');
			}
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }
}
