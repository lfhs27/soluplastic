<?php

namespace Soluplastic\Http\Controllers;

use Illuminate\Http\Request;
use Soluplastic\Product;
use Soluplastic\Order;
use Soluplastic\Facturas;
use DB;

class DashboardController extends Controller
{
    public function index()
    {
		$products = Product::all();
		
		$datedOrders = $this->getAllOrders();
		$datedFinalizadas = $this->getAllFinalizadas();
		$datedFacturas = $this->getAllFacturas();
		$datedAjustes = $this->getAllAjustes();
		
		$rentas = Order::where("type", "renta")->get();
		$ventas = Order::where("type", "venta")->get();
				
        return view("dashboard.index")
		->with("datedOrders", $datedOrders)
		->with("datedFinalizadas", $datedFinalizadas)
		->with("datedFacturas", $datedFacturas)
		->with("datedAjustes", $datedAjustes)
		->with("rentas", $rentas)
		->with("ventas", $ventas)
		->with("sidemenu", "dashboard");
    }
	
	private function getAllOrders(){
		$orders = Order::select(DB::raw('count(id) as `value`'), DB::raw("start_date as date"))
			->groupby('date')
			->get();
			
		return $orders;
	}
	
	private function getAllFinalizadas(){
		$orders = Order::select(DB::raw('count(id) as `value`'), DB::raw("date_finalizada as date"))
			->whereNotNull('date_finalizada')
			->groupby('date')
			->get();
			
		return $orders;
	}
	
	private function getAllFacturas(){
		$facturas = Facturas::select(DB::raw('count(id) as `value`'), DB::raw("created_at as date"))
			->groupby('date')
			->get();
			
		return $facturas;
	}
	
	private function getAllAjustes(){
		$orders = Order::select(DB::raw('count(id) as `value`'), DB::raw("created_at as date"))
			->groupby('date')
			->where('ajustes', '=', 1)
			->get();
			
		return $orders;
	}
}
