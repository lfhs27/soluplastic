<?php

namespace Soluplastic\Http\Controllers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Soluplastic\Door;

class DoorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
		$doors = Door::all();
		
        return view("dashboard.doors.index")
		->with("doors", $doors)
		->with("sidemenu", "puertas");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {		
		if (Gate::allows('crud_config', null)) {
			return view("dashboard.doors.create")
			->with("sidemenu", "puertas");
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
		if (Gate::allows('crud_config', null)) {
			$door = new Door;
						
			if($request->has("name"))
				$door->name = $request->name;
			else
				$door->name = "";
				
			$door->save();
			
			return redirect('/doors')->with('success', 'La puerta con el ID: '.$door->id.' ha sido creada.');
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
		$door = Door::find($id);
		
        return view("dashboard.doors.edit")
		->with("door", $door)
		->with("sidemenu", "puertas");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
		if (Gate::allows('crud_config', null)) {
			$door = Door::find($id);
			
			if($request->has("name"))
				$door->name = $request->name;
				
			$door->save();
			
			return redirect('/doors')->with('success', 'La puerta con el ID: '.$door->id.' ha sido modificada.');
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
		if (Gate::allows('crud_config', null)) {
			$door = Door::find($id);
			
			if($door){
				$door->destroy($id);
				return redirect('/doors')->with('success', 'La puerta ha sido eliminada.');
			}else{
				return redirect('/doors')->with('error', 'La puerta no se pudo eliminar');
			}
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }
}
