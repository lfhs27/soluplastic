<?php

namespace Soluplastic\Http\Controllers;

use DB;
use Mail;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Soluplastic\Ingreso;
use Soluplastic\Product;
use Soluplastic\Role;
use Soluplastic\User;

class IngresosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
		$ingresos = Ingreso::all();
		
        return view("dashboard.ingresos.index")
		->with("ingresos", $ingresos)
		->with("sidemenu", "ingresos");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {		
		if (Gate::allows('create_orders', null)) {
			$products = Product::select("id", "name as label")->get();
			
			return view("dashboard.ingresos.create")
			->with("products", $products)
			->with("sidemenu", "ingresos");
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
		if (Gate::allows('create_orders', null)) {
			$ingreso = new Ingreso;
						
			if($request->has("type"))
				$ingreso->type = $request->type;
				
			if($request->has("register_date"))
				$ingreso->register_date = $request->register_date;
				
			if($request->has("entry_date"))
				$ingreso->entry_date = $request->entry_date;
				
			if($request->has("provider"))
				$ingreso->provider = $request->provider;
				
			if($request->has("remision"))
				$ingreso->remision = $request->remision;
				
			if($request->has("comments"))
				$ingreso->comentarios = $request->comments;
			else
				$ingreso->comentarios = "";
							
			if($request->hasFile('file')){
				$factura = time().'.'.$request->file->getClientOriginalExtension();
				$request->file->move('ingresos', $factura);
				
				$ingreso->factura = $factura;
			}
						
			$ingreso->save();
			
			if($request->has("products")){
				foreach($request->products as $prod){
					$objProd = Product::find($prod["product_id"]);
					$ingreso->products()->save($objProd, [
														"quantity_verde" => $prod["quantity_verde"]?$prod["quantity_verde"]:"0",
														"quantity_azul" => $prod["quantity_azul"]?$prod["quantity_azul"]:"0",
														"quantity_amarillo" => $prod["quantity_amarillo"]?$prod["quantity_amarillo"]:"0",
														"quantity_naranja" => $prod["quantity_naranja"]?$prod["quantity_naranja"]:"0",
														"quantity_rojo" => $prod["quantity_rojo"]?$prod["quantity_rojo"]:"0",
														"devueltos" => $prod["quantity_devueltos"]?$prod["quantity_devueltos"]:"0",
														"comments" => $prod["comments"]?$prod["comments"]:""
														]);
					$objProd->inventario_verde += $prod["quantity_verde"]?$prod["quantity_verde"]:0;
					$objProd->inventario_azul += $prod["quantity_azul"]?$prod["quantity_azul"]:0;
					$objProd->inventario_amarillo += $prod["quantity_amarillo"]?$prod["quantity_amarillo"]:0;
					$objProd->inventario_naranja += $prod["quantity_naranja"]?$prod["quantity_naranja"]:0;
					$objProd->inventario_rojo += $prod["quantity_rojo"]?$prod["quantity_rojo"]:0;
					$objProd->save();
				}
			}
			
			$ingreso->save();
			
			return redirect('/entries')->with('success', 'El ingreso con el ID: '.$ingreso->id.' ha sido creado.');
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
		$ingreso = Ingreso::find($id);
		$products = Product::select("id", "name as label")->get();
		
        return view("dashboard.ingresos.edit")
		->with("ingreso", $ingreso)
		->with("products", $products)
		->with("sidemenu", "ingresos");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
		if (Gate::allows('update_orders', null)) {
			$ingreso = Ingreso::find($id);
						
			if($request->has("type"))
				$ingreso->type = $request->type;
				
			if($request->has("register_date"))
				$ingreso->register_date = $request->register_date;
				
			if($request->has("entry_date"))
				$ingreso->entry_date = $request->entry_date;
				
			if($request->has("provider"))
				$ingreso->provider = $request->provider;
				
			if($request->has("remision"))
				$ingreso->remision = $request->remision;
				
			if($request->has("comments"))
				$ingreso->comentarios = $request->comments;
			else
				$ingreso->comentarios = "";
							
			if($request->hasFile('file')){
				$factura = time().'.'.$request->file->getClientOriginalExtension();
				$request->file->move('ingresos', $factura);
				
				$ingreso->factura = $factura;
			}
						
			$ingreso->save();
								
			if($request->has("products")){
				$arr_prods = array();
				foreach($request->products as $prod){
					$arr_prods[$prod["product_id"]] = [
														"quantity_verde" => $prod["quantity_verde"]?$prod["quantity_verde"]:"0",
														"quantity_azul" => $prod["quantity_azul"]?$prod["quantity_azul"]:"0",
														"quantity_amarillo" => $prod["quantity_amarillo"]?$prod["quantity_amarillo"]:"0",
														"quantity_naranja" => $prod["quantity_naranja"]?$prod["quantity_naranja"]:"0",
														"quantity_rojo" => $prod["quantity_rojo"]?$prod["quantity_rojo"]:"0",
														"devueltos" => $prod["quantity_devueltos"]?$prod["quantity_devueltos"]:"0",
														"comments" => $prod["comments"]?$prod["comments"]:""
														];
				}
				$ingreso->products()->sync($arr_prods, false);
			}
			
			$ingreso->save();
			
			return redirect('/entries')->with('success', 'La orden con el ID: '.$ingreso->id.' ha sido modificada.');
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
		if (Gate::allows('delete_orders', null)) {
			$ingreso = Ingreso::find($id);
			
			if($ingreso){
				$ingreso->destroy($id);
				return redirect('/entries')->with('success', 'El ingreso ha sido eliminado.');
			}else{
				return redirect('/entries')->with('error', 'El ingreso no se pudo eliminar');
			}
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }
	
	public function sendmail($product){
		$role = Role::where(["signature" => "compras"])->first();
		if($role->users){
			$data = array(
				'product' => $product,
			);
			
			foreach($role->users as $user){
				$correo = $user->email;
				Mail::send('emails.notificacion', $data, function ($message) use ($correo) {

					$message->from('notificacionessoluplastic@gmail.com', 'Notificaciones Soluplastic');

					$message->to($correo)->subject('Aviso de unidad con poco inventario');

				});
			}
		}

		return true;
	}
}
