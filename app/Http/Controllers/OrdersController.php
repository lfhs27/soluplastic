<?php

namespace Soluplastic\Http\Controllers;

use DB;
use Mail;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Soluplastic\Order;
use Soluplastic\Product;
use Soluplastic\Role;
use Soluplastic\User;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
		$orders = Order::where("status", "!=", 1)->get();
		
        return view("dashboard.orders.index")
		->with("orders", $orders)
		->with("sidemenu", "ordenes");
    }
	
    public function liberadas()
    {
		$orders = Order::where("status", 1)->get();
		
        return view("dashboard.orders.index")
		->with("orders", $orders)
		->with("sidemenu", "ordenes");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {		
		if (Gate::allows('create_orders', null)) {
			$products = Product::all();
			
			return view("dashboard.orders.create")
			->with("products", $products)
			->with("sidemenu", "ordenes");
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
		if (Gate::allows('create_orders', null)) {
			$order = new Order;
						
			if($request->has("type"))
				$order->type = $request->type;
				
			if($request->has("start_date"))
				$order->start_date = $request->start_date;
				
			if($request->type != 'venta'){				
				if($request->has("end_date"))
					$order->end_date = $request->end_date;
			}
			
			if($request->hasFile('file')){
				$carta = time().'.'.$request->file->getClientOriginalExtension();
				$request->file->move('cartas', $carta);
				
				$order->file = $carta;
			}
			
			$order->user_id = Auth::id();
			
			$order->ajustes = $request->ajustes;
			$order->ajustes_comments = $request->ajustes_comments;
			$order->numero_cartaporte = $request->numero_cartaporte;
			$order->client = $request->client;
			$order->comments = $request->comments;
			
			if($request->hasFile('photo')){
				$photo = time().'.'.$request->photo->getClientOriginalExtension();
				$request->photo->move('orders/photos', $photo);
				
				$order->photo = $photo;
			}
			
			$order->save();
			
			$count = 0;
			if($request->has("products")){
				foreach($request->products as $prod){
					$objProd = Product::find($prod["product_id"]);
					$order->products()->save($objProd, ["quantity" => $prod["quantity"]]);
					
					if($objProd->availability($request->start_date) < $prod["quantity"])
						$count += $prod["quantity"] - $objProd->availability($request->start_date); 
						
					//send notification
					if($objProd->availability($request->start_date) - $objProd->separados($request->start_date)){
						$this->sendmail($objProd);
					}
				}
			}
			if($count == 0)	$order->separados = 1;
			
			if($request->estimated_time && is_numeric($request->estimated_time))
				$order->estimated_time = $request->estimated_time;
			
			$order->save();
			
			return redirect('/orders')->with('success', 'La orden con el ID: '.$order->id.' ha sido creada.');
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
		$order = Order::find($id);
		$products = Product::all();
		
        return view("dashboard.orders.edit")
		->with("order", $order)
		->with("products", $products)
		->with("sidemenu", "ordenes");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
		if (Gate::allows('update_orders', null)) {
			$order = Order::find($id);
						
			if($request->has("type"))
				$order->type = $request->type;
				
			if($request->has("start_date"))
				$order->start_date = $request->start_date;
					
			if($request->type != 'venta'){					
				if($request->has("end_date"))
					$order->end_date = $request->end_date;
			}else{
				$order->end_date = null;
			}
			
			if($request->hasFile('file')){
				$carta = time().'.'.$request->file->getClientOriginalExtension();
				$request->file->move('cartas', $carta);
				
				$order->file = $carta;
			}
			
			$order->ajustes = $request->ajustes;
			$order->ajustes_comments = $request->ajustes_comments;
			$order->numero_cartaporte = $request->numero_cartaporte;
			$order->client = $request->client;
			$order->comments = $request->comments;
			
			if($request->hasFile('photo')){
				$photo = time().'.'.$request->photo->getClientOriginalExtension();
				$request->photo->move('orders/photos', $photo);
				
				$order->photo = $photo;
			}
								
			if($request->has("products")){
				$arr_prods = array();
				foreach($request->products as $prod){
					$arr_prods[$prod["product_id"]] = ["quantity" => $prod["quantity"]];
					//$objProd = Product::find($prod["product_id"]);
					//$order->products()->save($objProd, ["quantity" => $prod["quantity"]]);
				}
				$order->products()->sync($arr_prods, false);
				
				
				//send notification
				$objProd = Product::find($prod["product_id"]);
				if($objProd->availability($request->start_date) - $objProd->separados($request->start_date)){
					$this->sendmail($objProd);
				}
			}
			
			if($request->estimated_time && is_numeric($request->estimated_time))
				$order->estimated_time = $request->estimated_time;
			
			$order->save();
			
			return redirect('/orders')->with('success', 'La orden con el ID: '.$order->id.' ha sido modificada.');
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
		if (Gate::allows('delete_orders', null)) {
			$order = Order::find($id);
			
			if($order){
				$order->destroy($id);
				return redirect('/orders')->with('success', 'La orden ha sido eliminado.');
			}else{
				return redirect('/orders')->with('error', 'La orden no se pudo eliminar');
			}
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }
	
    public function liberar($id)
	{
		if (Gate::denies('finalizar_orders', null)) {
			abort(403, 'Unauthorized action.');
		}
		
		$today = (new \Datetime())->format("Y-m-d");
		$order = Order::find($id);
		if($order && $order->status == 0){
			if($order->separados == 0)	return redirect('/orders')->with('error', 'La orden '.$id.' no ha sido separada.');
			
			if($order->type == "venta"){
				//Validate Inventory
				foreach($order->products as $product){
					if($product->availability($today, $order->id) < $product->pivot->quantity){
						return redirect('/orders')->with('error', 'No hay inventario suficiente para liberar la orden '.$id.'.');
					}
				}
				// Update inventory
				foreach($order->products as $product){
					if($product->availability($today, $order->id) >= $product->pivot->quantity){
						$requested = $product->pivot->quantity;
						$inventario = array(
								"inventario_verde", 
								"inventario_azul", 
								"inventario_amarillo", 
								"inventario_naranja"
							);
							
						$i = 0;
						while($i < COUNT($inventario)){
							if($product->{$inventario[$i]} > $requested){
								$product->{$inventario[$i]} -= $requested;
								$requested = 0;
								break;
							}else{
								$requested -= $product->{$inventario[$i]};
								$product->{$inventario[$i]} = 0;
								$i++;
							}
						}
						$product->save();
					}
				}
				$order->status = 1;
				$today = (new \Datetime())->format("Y-m-d");
				$order->date_finalizada = $today;
				$order->save();
				
				return redirect('/orders')->with('success', 'La orden '.$id.' ha sido liberada.');
			}else{
				$order->status = 1;
				$today = (new \Datetime())->format("Y-m-d");
				$order->date_finalizada = $today;

				$order->save();
				
				return redirect('/orders')->with('success', 'La orden '.$id.' ha sido liberada.');
			}
		}
		
		return redirect('/orders')->with('error', 'La orden '.$id.' no pudo ser liberada.');
	}
	
    public function separar($id)
	{
		if (Gate::denies('separar_orders', null)) {
			abort(403, 'Unauthorized action.');
		}
		
		$order = Order::find($id);
		if($order && $order->separados == 0){
			$order->separados = 1;
			$order->save();
		}
		
		return redirect('/orders')->with('success', 'La orden '.$id.' ha sido separada.');
	}
	
	public function estimate(Request $request){
		$products = $request->products;
		$cantidades = $request->cantidades;
		$idOrder = $request->idOrder ? $request->idOrder : null;
		//$start_date = $request->start_date;
		$start_date = $request->start_date;
		$estimate = 0;
		$invalid = false;
		
		if(COUNT($products)){
			foreach($products as $it => $id){
				$batchEstimate = 0;
				$carriedEstimate = 0;
				$deseadas = $cantidades[$it];
				$product = Product::find($id);
				$inventario = array(
								$product->inventario_verde, 
								$product->inventario_azul, 
								$product->inventario_amarillo, 
								$product->inventario_naranja
							);
				$tiempos = array(
								array($product->tiempo_verde_baja, $product->tiempo_verde_alta),
								array($product->tiempo_azul_baja, $product->tiempo_azul_alta),
								array($product->tiempo_amarillo_baja, $product->tiempo_amarillo_alta),
								array($product->tiempo_naranja_baja, $product->tiempo_naranja_alta)
							);
				$nOrdenesViejas = [0, 0, 0, 0];
				$nOrdenes = [0, 0, 0, 0];
				
				$orders = DB::table('products')
						->join('order_product', 'order_product.product_id', '=', 'products.id')
						->join('orders', 'orders.id', '=', 'order_product.order_id')
						->where('orders.status', '=', 0)
						->where('orders.separados', '=', 1)
						->where('orders.id', '!=', $idOrder)
						->where('products.id', '=', $product->id)
						->whereDate('orders.start_date', '<=', $start_date)
						//->select([DB::raw("SUM(order_product.quantity) as order_quantity")])
						->select([DB::raw("order_product.quantity as order_quantity")])
						->get();
				//dd($orders);
				$separadas = $orders ? $orders : [];
				
				if(!IS_NULL($separadas)){
					$i = 0;
					$j = 0;
					while($i < COUNT($inventario)){
						//echo $i." - entrar - separadas: ".$separadas." deseadas: ".$deseadas." inventario: ".$inventario[$i]."<br />";						
						/*if($separadas > 0){
							$nOrdenes[$i]++;
							if($separadas <= $inventario[$i]){
								$inventario[$i] -= $separadas;
								$separadas = 0;
							} else {
								$separadas -= $inventario[$i];
								$inventario[$i] = 0;
								$i++;
							}
						}	*/			
						if($j < COUNT($separadas)){
							$nOrdenesViejas[$i]++;
							if($separadas[$j]->order_quantity <= $inventario[$i]){
								if($separadas[$j]->order_quantity <= 100)	$carriedEstimate += $tiempos[$i][0];
								else										$carriedEstimate += $tiempos[$i][1];
								
								$inventario[$i] -= $separadas[$j]->order_quantity;
								$separadas[$j]->order_quantity = 0;
								$j++;
							} else {
								if($inventario[$i] <= 100)	$carriedEstimate += $tiempos[$i][0];
								else						$carriedEstimate +=  $tiempos[$i][1];
								
								$separadas[$j]->order_quantity -= $inventario[$i];
								$inventario[$i] = 0;
								$i++;
							}
						}
						//echo $i." - mitad - separadas: ".$separadas." deseadas: ".$deseadas." inventario: ".$inventario[$i]."<br />";
						if($deseadas == 0)	break;
						
						//if($inventario[$i] > 0 && $separadas == 0){
						if($inventario[$i] > 0 && $j >= COUNT($separadas)){
							$nOrdenes[$i]++;
							if($deseadas <= $inventario[$i]){
								if($deseadas <= 100)	$batchEstimate += $tiempos[$i][0];
								else					$batchEstimate += $tiempos[$i][1];
								
								$inventario[$i] -= $deseadas;
								$deseadas = 0;								
							} else {
								if($inventario[$i] <= 100)	$batchEstimate += $tiempos[$i][0];
								else						$batchEstimate += $tiempos[$i][1];
								
								$deseadas -= $inventario[$i];
								$inventario[$i] = 0;	
								$i++;
							}
							//$batchEstimate = $carriedEstimate;
							if($batchEstimate + $carriedEstimate > $estimate)	$estimate = $batchEstimate + $carriedEstimate;
						}
						//echo $i." - final - separadas: ".$separadas." deseadas: ".$deseadas." inventario: ".$inventario[$i]."<br />";
						if($deseadas == 0)	break;
					}
					//dd($nOrdenes);
				}
				if($deseadas != 0)
					$invalid = true;
			}
		}else{
			$invalid = true;
		}
		if(!$invalid)
			echo $estimate;
		else
			echo -1;
	}
	
	public function massive(Request $request){
		$msg = "";
		$action = $request->action;
		$orders = $request->ids;
		
		if($orders){
			foreach($orders as $idOrder){
				$order = Order::find($idOrder);
				
				switch($action){
					case 'finalizar':
						$order->status = 1;
						$today = (new \Datetime())->format("Y-m-d");
						$order->date_finalizada = $today;
						$order->save();
						
						break;
					case 'separar':
						$count = 0;
						if($order->status == 0 && $order->separados == 0){
							foreach($order->products as $product){
								if($product->availability($order->start_date) < $product->pivot->quantity){
									$count += $product->pivot->quantity - $product->availability($order->start_date); 
								}
							}
							
							if($count == 0){
								$order->separados = 1;
								$order->save();
							}
						}
						
						break;
				}
			}
			
			$msg = "Las ordenes: ".IMPLODE(', ', $orders)." han sido actualizadas.";
		}
		
		return redirect('/orders')->with('success', $msg);
	}
	
	public function sendmail($product){
		$role = Role::where(["signature" => "compras"])->first();
		if($role->users){
			$data = array(
				'product' => $product,
			);
			
			foreach($role->users as $user){
				$correo = $user->email;
				Mail::send('emails.notificacion', $data, function ($message) use ($correo) {

					$message->from('notificacionessoluplastic@gmail.com', 'Notificaciones Soluplastic');

					$message->to($correo)->subject('Aviso de unidad con poco inventario');

				});
			}
		}

		return true;
	}
}
