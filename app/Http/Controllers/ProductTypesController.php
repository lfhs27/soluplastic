<?php

namespace Soluplastic\Http\Controllers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Soluplastic\ProductType;

class ProductTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
		$types = ProductType::all();
		
        return view("dashboard.producttypes.index")
		->with("types", $types)
		->with("sidemenu", "tipos");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {		
		if (Gate::allows('crud_config', null)) {
			return view("dashboard.producttypes.create")
			->with("sidemenu", "tipos");
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
		if (Gate::allows('crud_config', null)) {
			$type = new ProductType;
						
			if($request->has("name"))
				$type->name = $request->name;
			else
				$type->name = "";
				
			$type->save();
			
			return redirect('/product-types')->with('success', 'El Tipo de Producto con el ID: '.$type->id.' ha sido creado.');
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
		$type = ProductType::find($id);
		
        return view("dashboard.producttypes.edit")
		->with("type", $type)
		->with("sidemenu", "tipos");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
		if (Gate::allows('crud_config', null)) {
			$type = ProductType::find($id);
			
			if($request->has("name"))
				$type->name = $request->name;
				
			$type->save();
			
			return redirect('/product-types')->with('success', 'El Tipo de Producto con el ID: '.$type->id.' ha sido modificado.');
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
		if (Gate::allows('crud_config', null)) {
			$type = ProductType::find($id);
			
			if($type){
				$type->destroy($id);
				return redirect('/product-types')->with('success', 'El Tipo de Producto ha sido eliminado.');
			}else{
				return redirect('/product-types')->with('error', 'El Tipo de Producto no se pudo eliminar');
			}
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }
}
