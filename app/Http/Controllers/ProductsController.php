<?php

namespace Soluplastic\Http\Controllers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Soluplastic\Brand;
use Soluplastic\Product;
use Soluplastic\Color;
use Soluplastic\Base;
use Soluplastic\Door;
use Soluplastic\Category;
use Soluplastic\Classification;
use Soluplastic\ProductType;
use Soluplastic\CartaPorte;
use Soluplastic\ProductPhotos;
use Soluplastic\Facturas;
use Excel;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
		$products = Product::all();
		
        return view("dashboard.products.index")
		->with("products", $products)
		->with("sidemenu", "productos");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {		
		if (Gate::allows('crud_products', null)) {
			$brands = Brand::all();
			$productTypes = ProductType::all();
			$colors = Color::all();
			$bases = Base::all();
			$doors = Door::all();
			$categories = Category::all();
			$classifications = classification::all();
			
			return view("dashboard.products.create")
			->with("brands", $brands)
			->with("colors", $colors)
			->with("bases", $bases)
			->with("doors", $doors)
			->with("categories", $categories)
			->with("classifications", $classifications)
			->with("productTypes", $productTypes)
			->with("sidemenu", "productos");
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
		if (Gate::allows('crud_products', null)) {
			$product = new Product;
						
			if($request->has("name"))
				$product->name = $request->name;
			else
				$product->name = "";
			
			$product->product_type_id = $request->product_type_id;
			$product->brand_id = $request->brand_id;
			$product->color_id = $request->color_id;
			$product->base_id = $request->base_id;
			$product->door_id = $request->door_id;
			$product->category_id = $request->category_id;
			$product->classification_id = $request->classification_id;
			$product->status = $request->status;
			
			if($request->has("inventario_verde"))
				$product->inventario_verde = $request->inventario_verde;
			
			if($request->has("inventario_azul"))
				$product->inventario_azul = $request->inventario_azul;
			
			if($request->has("inventario_amarillo"))
				$product->inventario_amarillo = $request->inventario_amarillo;
			
			if($request->has("inventario_naranja"))
				$product->inventario_naranja = $request->inventario_naranja;
			
			if($request->has("inventario_rojo"))
				$product->inventario_rojo = $request->inventario_rojo;
				
			if($request->has("quantity_alert"))
				$product->quantity_alert = $request->quantity_alert;
				
			$product->tiempo_verde_baja = $request->tiempo_verde_baja;
			$product->tiempo_verde_alta = $request->tiempo_verde_alta;
			$product->tiempo_azul_baja = $request->tiempo_azul_baja;
			$product->tiempo_azul_alta = $request->tiempo_azul_alta;
			$product->tiempo_amarillo_baja = $request->tiempo_amarillo_baja;
			$product->tiempo_amarillo_alta = $request->tiempo_amarillo_alta;
			$product->tiempo_naranja_baja = $request->tiempo_naranja_baja;
			$product->tiempo_naranja_alta = $request->tiempo_naranja_alta;
				
			$product->width = $request->width;
			$product->height = $request->height;
			$product->length = $request->length;
				
			$product->save();
			
			if($request->hasFile('photo_file')){
				$photo = time().'.'.$request->photo_file->getClientOriginalExtension();
				$request->photo_file->move('photos/products', $photo);
				
				$productPhoto = new ProductPhotos;
				$productPhoto->path = $photo;
				$productPhoto->product_id = $product->id;
				$productPhoto->save();
			}
			
			return redirect('/productos')->with('success', 'El producto con el ID: '.$product->id.' ha sido creado.');
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
		$product = Product::find($id);
		$brands = Brand::all();
		$productTypes = ProductType::all();
		$colors = Color::all();
		$bases = Base::all();
		$doors = Door::all();
		$categories = Category::all();
		$classifications = classification::all();
		
		$calendar = array();
		foreach($product->orders as $order){
			$evento = new \stdClass();
			$evento->title = ucfirst($order->type)." (".$order->pivot->quantity." unidades)";
			$evento->start = $order->start_date;
			if($order->end_date)
				$evento->end = (new \Datetime($order->end_date))->modify("+1 day")->format("Y-m-d");
				
			if($order->type == "renta")	$evento->color = "#F8CB00";
			else						$evento->color = "#F3565D";
			$evento->url = action("OrdersController@edit", $order->id);
			array_push($calendar, $evento);
		}
		//dd($calendar);
        return view("dashboard.products.edit")
		->with("product", $product)
		->with("brands", $brands)
		->with("colors", $colors)
		->with("bases", $bases)
		->with("doors", $doors)
		->with("calendar", $calendar)
		->with("categories", $categories)
		->with("classifications", $classifications)
		->with("productTypes", $productTypes)
		->with("sidemenu", "productos");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
		if (Gate::allows('crud_products', null)) {
			$product = Product::find($id);
			
			if($request->has("name"))
				$product->name = $request->name;
					
			if($request->has("product_type_id"))
				$product->product_type_id = $request->product_type_id;
			
			if($request->has("brand_id"))
				$product->brand_id = $request->brand_id;
			
			if($request->has("color_id"))
				$product->color_id = $request->color_id;
			
			if($request->has("base_id"))
				$product->base_id = $request->base_id;
			
			if($request->has("door_id"))
				$product->door_id = $request->door_id;
			
			if($request->has("category_id"))
				$product->category_id = $request->category_id;
			
			if($request->has("classification_id"))
				$product->classification_id = $request->classification_id;
			
			if($request->has("status"))
				$product->status = $request->status;
			
			if($request->has("quantity"))
				$product->quantity = $request->quantity;
			
			if($request->has("inventario_verde"))
				$product->inventario_verde = $request->inventario_verde;
			
			if($request->has("inventario_azul"))
				$product->inventario_azul = $request->inventario_azul;
			
			if($request->has("inventario_amarillo"))
				$product->inventario_amarillo = $request->inventario_amarillo;
			
			if($request->has("inventario_naranja"))
				$product->inventario_naranja = $request->inventario_naranja;
			
			if($request->has("inventario_rojo"))
				$product->inventario_rojo = $request->inventario_rojo;
								
			if($request->has("tiempo_verde_baja"))
				$product->tiempo_verde_baja = $request->tiempo_verde_baja;
			if($request->has("tiempo_verde_alta"))
				$product->tiempo_verde_alta = $request->tiempo_verde_alta;
			if($request->has("tiempo_azul_baja"))
				$product->tiempo_azul_baja = $request->tiempo_azul_baja;
			if($request->has("tiempo_azul_alta"))
				$product->tiempo_azul_alta = $request->tiempo_azul_alta;
			if($request->has("tiempo_amarillo_baja"))
				$product->tiempo_amarillo_baja = $request->tiempo_amarillo_baja;
			if($request->has("tiempo_amarillo_alta"))
				$product->tiempo_amarillo_alta = $request->tiempo_amarillo_alta;
			if($request->has("tiempo_naranja_baja"))
				$product->tiempo_naranja_baja = $request->tiempo_naranja_baja;
			if($request->has("tiempo_naranja_alta"))
				$product->tiempo_naranja_alta = $request->tiempo_naranja_alta;
			
			if($request->has("width"))
				$product->width = $request->width;
			
			if($request->has("height"))
				$product->height = $request->height;
			
			if($request->has("length"))
				$product->length = $request->length;
			
			if($request->has("quantity_alert"))
				$product->quantity_alert = $request->quantity_alert;
			
			if($request->hasFile('carta_porte_file')){
				$carta = time().'.'.$request->carta_porte_file->getClientOriginalExtension();
				$request->carta_porte_file->move('cartas/products', $carta);
				
				$cartaPorte = new CartaPorte;
				$cartaPorte->fecha = $request->carta_porte_fecha;
				$cartaPorte->file = $carta;
				$cartaPorte->product_id = $product->id;
				$cartaPorte->user_id = Auth::user()->id;
				$cartaPorte->save();
			}
			
			if($request->hasFile('photo_file')){
				$photo = time().'.'.$request->photo_file->getClientOriginalExtension();
				$request->photo_file->move('photos/products', $photo);
				
				$productPhoto = new ProductPhotos;
				$productPhoto->path = $photo;
				$productPhoto->product_id = $product->id;
				$productPhoto->save();
			}
			if($request->hasFile('factura_file')){
				$fact = time().'.'.$request->factura_file->getClientOriginalExtension();
				$request->factura_file->move('facturas/products', $fact);
				
				$factura = new Facturas;
				$factura->path = $fact;
				$factura->product_id = $product->id;
				$factura->numero = $request->factura_numero;
				$factura->fecha = $request->factura_fecha;
				$factura->devueltos = $request->factura_devueltos;
				$factura->comments = $request->factura_comments;
				$factura->quantity = $request->factura_quantity;
				$factura->save();
			}
				
			$product->save();
			
			return redirect('/productos')->with('success', 'El producto con el ID: '.$product->id.' ha sido modificado.');
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
		if (Gate::allows('crud_products', null)) {
			$product = Product::find($id);
			
			if($product){
				$product->destroy($id);
				return redirect('/productos')->with('success', 'El producto ha sido eliminado.');
			}else{
				return redirect('/productos')->with('error', 'El producto no se pudo eliminar');
			}
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }
	
	public function import(Request $request){
		if($request->hasFile('import_file')){
			$import = time().'.'.$request->import_file->getClientOriginalExtension();
			$request->import_file->move('products/excel', $import);
			
			
			Excel::load('products/excel/'.$import, function($reader) {

				// Getting all results
				$results = $reader->get();

				// ->all() is a wrapper for ->get() and will work the same
				$results = $reader->all();
				
				$reader->each(function($sheet) {
					// Loop through all rows
					$sheet->each(function($row) {
						//dd($row);
						
						$product_type = ProductType::whereRaw("LOWER(name) like ?", array($row->tipo_producto))->first();
						if(!$product_type){
							$product_type = new ProductType;
							$product_type->name = ucwords($row->tipo_producto);
							$product_type->save();
						}
						
						$brand = Brand::whereRaw("LOWER(name) like ?", array(strtolower($row->marca)))->first();
						if(!$brand){
							$brand = new Brand;
							$brand->name = ucwords(strtolower($row->marca));
							$brand->save();
						}
						
						$color = Color::whereRaw("LOWER(name) like ?", array(strtolower($row->color)))->first();
						if(!$color){
							$color = new Color;
							$color->name = ucwords(strtolower($row->color));
							$color->save();
						}
						
						$base = Base::whereRaw("LOWER(name) like ?", array(strtolower($row->base)))->first();
						if(!$base){
							$base = new Base;
							$base->name = ucwords(strtolower($row->base));
							$base->save();
						}
						
						$door = Door::whereRaw("LOWER(name) like ?", array(strtolower($row->puerta)))->first();
						if(!$door){
							$door = new Door;
							$door->name = ucwords(strtolower($row->puerta));
							$door->save();
						}
						
						$category = Category::whereRaw("LOWER(name) like ?", array(strtolower($row->categoria)))->first();
						if(!$category){
							$category = new Category;
							$category->name = ucwords(strtolower($row->categoria));
							$category->save();
						}
						
						$classification = Classification::whereRaw("LOWER(name) like ?", array(strtolower($row->clasificacion)))->first();
						if(!$classification){
							$classification = new Classification;
							$classification->name = ucwords(strtolower($row->clasificacion));
							$classification->save();
						}
						
						
						$product = new Product;
							
						$product->name = $row->nombre;
											
						$product->product_type_id = $product_type->id;
						$product->brand_id = $brand->id;
						$product->color_id = $color->id;
						$product->base_id = $base->id;
						$product->door_id = $door->id;
						$product->category_id = $category->id;
						$product->classification_id = $classification->id;
						
						$product->inventario_verde = $row->inventario_verde ? $row->inventario_verde : 0;
						$product->inventario_azul = $row->inventario_azul ? $row->inventario_azul : 0;
						$product->inventario_amarillo = $row->inventario_amarillo ? $row->inventario_amarillo : 0;
						$product->inventario_naranja = $row->inventario_naranja ? $row->inventario_naranja : 0;
						$product->inventario_rojo = $row->inventario_rojo ? $row->inventario_rojo : 0;
							
						$product->quantity_alert = $row->alerta_inventario ? $row->alerta_inventario : 0;
							
						$product->tiempo_verde_baja = $row->tiempo_verde_baja ? $row->tiempo_verde_baja : 1;
						$product->tiempo_verde_alta = $row->tiempo_verde_alta ? $row->tiempo_verde_alta : 1;
						$product->tiempo_azul_baja = $row->tiempo_azul_baja ? $row->tiempo_azul_baja : 1;
						$product->tiempo_azul_alta = $row->tiempo_azul_alta ? $row->tiempo_azul_alta : 1;
						$product->tiempo_amarillo_baja = $row->tiempo_amarillo_baja ? $row->tiempo_amarillo_baja : 1;
						$product->tiempo_amarillo_alta = $row->tiempo_amarillo_alta ? $row->tiempo_amarillo_alta : 1;
						$product->tiempo_naranja_baja = $row->tiempo_naranja_baja ? $row->tiempo_naranja_baja : 1;
						$product->tiempo_naranja_alta = $row->tiempo_naranja_alta ? $row->tiempo_naranja_alta : 1;
							
						$product->width = $row->width ? $row->width : 1;
						$product->height = $row->height ? $row->height : 1;
						$product->length = $row->length ? $row->length : 1;
							
						$product->save();
					});
					
				});

			});
		}
		return redirect('/productos')->with('success', 'Los productos han sido importados.');
	}
}
