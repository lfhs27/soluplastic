<?php

namespace Soluplastic\Http\Controllers;

use Illuminate\Http\Request;
use Soluplastic\Ingreso;

class ReportsController extends Controller
{
    public function entries(Request $request)
    {
		$query = $request->q;
		
		$ingresos = null;
		if(IS_NULL($query))
			$ingresos = Ingreso::all();
		else
			$ingresos = Ingreso::Where('register_date', 'like', '%' . $query . '%')
								->orWhere('entry_date', 'like', '%' . $query . '%')
								->orWhere('remision', 'like', '%' . $query . '%')->get();
		
        return view("dashboard.reports.entries")
		->with("ingresos", $ingresos)
		->with("sidemenu", "reportes");
    }
}
