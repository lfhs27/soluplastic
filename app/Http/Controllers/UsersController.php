<?php

namespace Soluplastic\Http\Controllers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Soluplastic\User;
use Soluplastic\Role;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
		$users = User::all();
		
        return view("dashboard.users.index")
		->with("users", $users)
		->with("sidemenu", "usuarios");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {		
		if (Gate::allows('crud_users', null)) {
			$roles = Role::all();
			
			return view("dashboard.users.create")
			->with("roles", $roles)
			->with("sidemenu", "usuarios");
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
		if (Gate::allows('crud_users', null)) {
			$user = new User;
						
			if($request->has("name"))
				$user->name = $request->name;
			else
				$user->name = "";
						
			if($request->has("email"))
				$user->email = $request->email;
						
			$user->password = bcrypt($request->password);
			$user->save();
			
			
			$user->roles()->attach($request->roles);
							
			return redirect('/users')->with('success', 'El usuario con el ID: '.$user->id.' ha sido creado.');
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
		$user = User::find($id);
		$roles = Role::all();
		
        return view("dashboard.users.edit")
		->with("user", $user)
		->with("roles", $roles)
		->with("sidemenu", "usuarios");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
		if (Gate::allows('crud_users', null)) {
			$user = User::find($id);
						
			if($request->has("name"))
				$user->name = $request->name;
			else
				$user->name = "";
						
			if($request->has("email"))
				$user->email = $request->email;
						
			if($request->has("password"))
				$user->password = bcrypt($request->password);
			
			$user->save();
			
			
			$user->roles()->sync($request->roles);
			
			return redirect('/users')->with('success', 'El usuario con el ID: '.$user->id.' ha sido modificado.');
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
		if (Gate::allows('crud_users', null)) {
			$user = User::find($id);
			
			if($user){
				$user->destroy($id);
				return redirect('/users')->with('success', 'El user ha sido eliminado.');
			}else{
				return redirect('/users')->with('error', 'El user no se pudo eliminar');
			}
		}else{
			echo "No tienes permiso para ver esta página.";
		}
    }
}
