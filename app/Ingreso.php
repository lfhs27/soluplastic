<?php

namespace Soluplastic;

use Illuminate\Database\Eloquent\Model;

class Ingreso extends Model
{
    protected $table = "ingresos";	
	
	public function products()
	{
		return $this->belongsToMany('Soluplastic\Product', 'ingreso_product')
			->withPivot('quantity_verde', 'quantity_azul', 'quantity_amarillo', 'quantity_rojo', 'quantity_naranja', 'devueltos', 'comments')
			->withTimestamps();
	}
	
	public function total(){
		return $this->quantity_verde + $this->quantity_azul + $this->quantity_amarillo + $this->quantity_rojo + $this->quantity_naranja + $this->devueltos;
	}
}
