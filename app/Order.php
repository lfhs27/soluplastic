<?php

namespace Soluplastic;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	
    public function user()
    {
    	return $this->belongsTo('Soluplastic\User');
    }
	
	public function products()
	{
		return $this->belongsToMany('Soluplastic\Product', 'order_product')
			->withPivot('quantity')
			->withTimestamps();
	}
	
	public function total_products(){
		$count = 0;
		foreach($this->products as $product){
			$count += $product->pivot->quantity;
		}
		
		return $count;
	}
}
