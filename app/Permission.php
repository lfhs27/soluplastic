<?php

namespace Soluplastic;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public function roles()
    {
    	return $this->belongsToMany('Soluplastic\Role');
    }
}
