<?php

namespace Soluplastic;

use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model
{
    public function product_type()
    {
    	return $this->belongsTo('Soluplastic\ProductType');
    }
	
    public function brand()
    {
    	return $this->belongsTo('Soluplastic\Brand');
    }
	
    public function color()
    {
    	return $this->belongsTo('Soluplastic\Color');
    }
	
    public function base()
    {
    	return $this->belongsTo('Soluplastic\Base');
    }
	
    public function door()
    {
    	return $this->belongsTo('Soluplastic\Door');
    }
	
    public function classification()
    {
    	return $this->belongsTo('Soluplastic\Classification');
    }
	
    public function category()
    {
    	return $this->belongsTo('Soluplastic\Categories');
    }
	
	public function orders()
	{
		return $this->belongsToMany('Soluplastic\Order', 'order_product')
			->withPivot('quantity')
			->withTimestamps();
	}
	
	public function ingresos()
	{
		return $this->belongsToMany('Soluplastic\Ingreso', 'ingreso_product')
			->withPivot('quantity_verde', 'quantity_azul', 'quantity_amarillo', 'quantity_rojo', 'devueltos', 'comments')
			->withTimestamps();
	}
	
    public function carta_portes()
    {
    	return $this->hasMany('Soluplastic\CartaPorte');
    }
	
    public function photos()
    {
    	return $this->hasMany('Soluplastic\ProductPhotos');
    }
	
    public function facturas()
    {
    	return $this->hasMany('Soluplastic\Facturas');
    }
	
	public function inventario_total(){
		return $this->inventario_verde + $this->inventario_azul + $this->inventario_amarillo + $this->inventario_naranja;
	}
	
	public function separados($date, $idOrder = null){
		DB::enableQueryLog();
		$apartados = DB::table('products')
					->join('order_product', 'products.id', '=', 'order_product.product_id')
					->join('orders', 'orders.id', '=', 'order_product.order_id')
					->where('products.id', '=', $this->id)
					->where('orders.status', '=', 0)
					->where('orders.separados', '=', 1)
					->where('orders.id', '!=', $idOrder)
					->where('orders.start_date', '<=', $date)
					->select(DB::raw('SUM(order_product.quantity) as separados'))
					->get();
					
		return IS_NULL($apartados[0]->separados) ? 0 : $apartados[0]->separados;
	}
	
	public function availability($date, $idOrder = null){
		DB::enableQueryLog();
		$apartados = DB::table('products')
					->join('order_product', 'products.id', '=', 'order_product.product_id')
					->join('orders', 'orders.id', '=', 'order_product.order_id')
					->where('products.id', '=', $this->id)
					->where('orders.status', '=', 0)
					->where('orders.separados', '=', 1)
					->where('orders.id', '!=', $idOrder)
					->where('orders.start_date', '<=', $date)
					->select(DB::raw('SUM(order_product.quantity) as separados'))
					->get();
		
		$availability = $this->inventario_total() - (IS_NULL($apartados[0]->separados) ? 0 : $apartados[0]->separados);
		
		return $availability;
	}
	
	public function rentados($date){
		DB::enableQueryLog();
		$apartados = DB::table('products')
					->join('order_product', 'products.id', '=', 'order_product.product_id')
					->join('orders', 'orders.id', '=', 'order_product.order_id')
					->where('products.id', '=', $this->id)
					->where('orders.type', '!=', 'venta')
					->where('orders.status', '=', 0)
					->where('orders.separados', '=', 1)
					->where('orders.start_date', '<=', $date)
					->select(DB::raw('SUM(order_product.quantity) as separados'))
					->get();
					
		return IS_NULL($apartados[0]->separados) ? 0 : $apartados[0]->separados;
	}
}
