<?php

namespace Soluplastic;

use Illuminate\Database\Eloquent\Model;

class ProductPhotos extends Model
{
    protected $table = "product_photos";
}
