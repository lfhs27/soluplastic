<?php

namespace Soluplastic\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Soluplastic\Permission;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'Soluplastic\Model' => 'Soluplastic\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
		$this->registerPolicies($gate);

		foreach (Permission::all() as $permission) {
			$gate->define($permission->signature, function ($user) use ($permission) {
				return $user->hasRole($permission->roles);
			});
		}
    }
}
