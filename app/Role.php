<?php

namespace Soluplastic;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function permissions()
    {
    	return $this->belongsToMany('Soluplastic\Permission');
    }
    public function users()
    {
    	return $this->belongsToMany('Soluplastic\User');
    }
}
