<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
			
			$table->string("name", 255);
			
			$table->integer("product_type_id")->unsigned();
			$table->integer("brand_id")->unsigned();
			$table->integer("color_id")->unsigned();
			$table->integer("base_id")->unsigned();
			$table->integer("door_id")->unsigned();
			$table->integer("category_id")->unsigned();
			$table->integer("classification_id")->unsigned();
					
			$table->integer("status")->default(1);
			$table->integer("quantity")->default(0);
			
			$table->float("width")->default(0);
			$table->float("height")->default(0);
			$table->float("length")->default(0);
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
