<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->foreign('product_type_id')
					->references('id')->on('product_types')
					->onDelete('cascade');
			
			$table->foreign('brand_id')
					->references('id')->on('brands')
					->onDelete('cascade');
					
			$table->foreign('color_id')
					->references('id')->on('colors')
					->onDelete('cascade');
					
			$table->foreign('base_id')
					->references('id')->on('bases')
					->onDelete('cascade');
					
			$table->foreign('door_id')
					->references('id')->on('doors')
					->onDelete('cascade');
					
			$table->foreign('category_id')
					->references('id')->on('categories')
					->onDelete('cascade');
					
			$table->foreign('classification_id')
					->references('id')->on('classifications')
					->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            //
        });
    }
}
