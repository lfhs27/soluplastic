<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTiemposToProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->float("tiempo_verde_baja");
            $table->float("tiempo_verde_alta");
			
            $table->float("tiempo_azul_baja");
            $table->float("tiempo_azul_alta");
			
            $table->float("tiempo_amarillo_baja");
            $table->float("tiempo_amarillo_alta");
			
            $table->float("tiempo_naranja_baja");
            $table->float("tiempo_naranja_alta");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            //
        });
    }
}
