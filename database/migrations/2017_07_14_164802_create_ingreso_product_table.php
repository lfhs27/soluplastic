<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngresoProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingreso_product', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
			
			$table->integer('ingreso_id')->unsigned();
			$table->foreign('ingreso_id')
					->references('id')->on('ingresos')
					->onDelete('cascade');
			
			$table->integer('product_id')->unsigned();
			$table->foreign('product_id')
					->references('id')->on('products')
					->onDelete('cascade');
					
			$table->integer("quantity_verde")->default('0');
			$table->integer("quantity_azul")->default('0');
			$table->integer("quantity_amarillo")->default('0');
			$table->integer("quantity_rojo")->default('0');
			$table->integer("devueltos")->default('0');
			$table->text("comments")->nullable()->default(null);
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingreso_product');
    }
}
