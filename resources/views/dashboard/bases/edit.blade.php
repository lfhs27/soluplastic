@extends('layouts.dashboard')

@section('content')
<div class="page-content">
	<!-- BEGIN PAGE HEADER-->
	<h1 class="page-title">
		Editar Base
		<!--small>subheader</small-->
	</h1>
	<!-- END PAGE HEADER-->
	<form action="{{ action('BasesController@update', $base->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
		{{ method_field('PUT') }}
		{{ csrf_field() }}
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<div class="portlet light">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-bubble font-dark hide"></i>
							<span class="caption-subject font-hide bold uppercase">Información de la Base</span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Nombre</label>
								<div class="col-sm-8">
									<input type="text" name="name" class="form-control" id="" value="{{ $base->name }}">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<input type="submit" class="btn btn-success" value="Guardar" style="width:100%;" />
			</div>
		</div>
	</form>
</div>

<script>
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip(); 
});
</script>

@endsection