@extends('layouts.dashboard')

@section('content')
<div class="page-content">
	<!-- BEGIN PAGE HEADER-->
	<h1 class="page-title"> {{ ucfirst($sidemenu) }}
		<!--small>subheader</small-->
	</h1>
	<div class="page-bar">
		<ul class="page-breadcrumb">
			<li>
				<i class="icon-home"></i>
				<a href="index.html">Home</a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<span>Dashboard</span>
			</li>
		</ul>
	</div>
	<!-- END PAGE HEADER-->
	@if(session('success'))
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				{{ session('success') }}
			</div>
		</div>
	</div>
	@endif
	@if(session('error'))
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				{{ session('error') }}
			</div>
		</div>
	</div>
	@endif
	<div class="row">
		<div class="col-md-6 col-sm-12">
			<div class="portlet light ">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-bubble font-dark hide"></i>
						<span class="caption-subject font-hide bold uppercase">Salidas</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-xs-12">
							<div id="chart_1" class="chart" style="height: 500px;"> </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-sm-12">
			<div class="portlet light ">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-bubble font-dark hide"></i>
						<span class="caption-subject font-hide bold uppercase">Entradas</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-xs-12">
							<div id="chart_2" class="chart" style="height: 500px;"> </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-sm-12">
			<div class="portlet light ">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-bubble font-dark hide"></i>
						<span class="caption-subject font-hide bold uppercase">Finalizadas</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-xs-12">
							<div id="chart_finalizadas" class="chart" style="height: 500px;"> </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-sm-12">
			<div class="portlet light ">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-bubble font-dark hide"></i>
						<span class="caption-subject font-hide bold uppercase">Ajustes</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-xs-12">
							<div id="chart_3" class="chart" style="height: 500px;"> </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-sm-12">
			<div class="portlet light ">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-bubble font-dark hide"></i>
						<span class="caption-subject font-hide bold uppercase">Rentas/Ventas</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-xs-12">
							<div id="chart_6" class="chart" style="height: 500px;"> </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip(); 
	
	AmCharts.makeChart("chart_1", {
		"type": "serial",
		"theme": "light",
		"marginRight": 40,
		"marginLeft": 40,
		"autoMarginOffset": 20,
		"mouseWheelZoomEnabled":true,
		"dataDateFormat": "YYYY-MM-DD",
		"valueAxes": [{
			"id": "v1",
			"axisAlpha": 0,
			"position": "left",
			"ignoreAxisWidth":true,
			"precision": 0
		}],
		"balloon": {
			"borderThickness": 1,
			"shadowAlpha": 0
		},
		"graphs": [{
			"id": "g1",
			"balloon":{
			  "drop":true,
			  "adjustBorderColor":false,
			  "color":"#ffffff"
			},
			"bullet": "round",
			"bulletBorderAlpha": 1,
			"bulletColor": "#FFFFFF",
			"bulletSize": 5,
			"hideBulletsCount": 50,
			"lineThickness": 2,
			"title": "red line",
			"useLineColorForBulletBorder": true,
			"valueField": "value",
			"balloonText": "<span style='font-size:18px;'>[[value]]</span>"
		}],
		"chartScrollbar": {
			"graph": "g1",
			"oppositeAxis":false,
			"offset":30,
			"scrollbarHeight": 80,
			"backgroundAlpha": 0,
			"selectedBackgroundAlpha": 0.1,
			"selectedBackgroundColor": "#888888",
			"graphFillAlpha": 0,
			"graphLineAlpha": 0.5,
			"selectedGraphFillAlpha": 0,
			"selectedGraphLineAlpha": 1,
			"autoGridCount":true,
			"color":"#AAAAAA"
		},
		"chartCursor": {
			"pan": true,
			"valueLineEnabled": true,
			"valueLineBalloonEnabled": true,
			"cursorAlpha":1,
			"cursorColor":"#258cbb",
			"limitToGraph":"g1",
			"valueLineAlpha":0.2,
			"valueZoomable":true
		},
		"valueScrollbar":{
		  "oppositeAxis":false,
		  "offset":50,
		  "scrollbarHeight":10
		},
		"categoryField": "date",
		"categoryAxis": {
			"parseDates": true,
			"dashLength": 1,
			"minorGridEnabled": true
		},
		"export": {
			"enabled": true
		},
		"dataProvider": {!! json_encode($datedOrders) !!}
	});
	AmCharts.makeChart("chart_2", {
		"type": "serial",
		"theme": "light",
		"marginRight": 40,
		"marginLeft": 40,
		"autoMarginOffset": 20,
		"mouseWheelZoomEnabled":true,
		"dataDateFormat": "YYYY-MM-DD",
		"valueAxes": [{
			"id": "v1",
			"axisAlpha": 0,
			"position": "left",
			"ignoreAxisWidth":true,
			"precision": 0
		}],
		"balloon": {
			"borderThickness": 1,
			"shadowAlpha": 0
		},
		"graphs": [{
			"id": "g1",
			"balloon":{
			  "drop":true,
			  "adjustBorderColor":false,
			  "color":"#ffffff"
			},
			"bullet": "round",
			"bulletBorderAlpha": 1,
			"bulletColor": "#FFFFFF",
			"bulletSize": 5,
			"hideBulletsCount": 50,
			"lineThickness": 2,
			"title": "red line",
			"useLineColorForBulletBorder": true,
			"valueField": "value",
			"balloonText": "<span style='font-size:18px;'>[[value]]</span>"
		}],
		"chartScrollbar": {
			"graph": "g1",
			"oppositeAxis":false,
			"offset":30,
			"scrollbarHeight": 80,
			"backgroundAlpha": 0,
			"selectedBackgroundAlpha": 0.1,
			"selectedBackgroundColor": "#888888",
			"graphFillAlpha": 0,
			"graphLineAlpha": 0.5,
			"selectedGraphFillAlpha": 0,
			"selectedGraphLineAlpha": 1,
			"autoGridCount":true,
			"color":"#AAAAAA"
		},
		"chartCursor": {
			"pan": true,
			"valueLineEnabled": true,
			"valueLineBalloonEnabled": true,
			"cursorAlpha":1,
			"cursorColor":"#258cbb",
			"limitToGraph":"g1",
			"valueLineAlpha":0.2,
			"valueZoomable":true
		},
		"valueScrollbar":{
		  "oppositeAxis":false,
		  "offset":50,
		  "scrollbarHeight":10
		},
		"categoryField": "date",
		"categoryAxis": {
			"parseDates": true,
			"dashLength": 1,
			"minorGridEnabled": true
		},
		"export": {
			"enabled": true
		},
		"dataProvider": {!! json_encode($datedFacturas) !!}
	});
	AmCharts.makeChart("chart_3", {
		"type": "serial",
		"theme": "light",
		"marginRight": 40,
		"marginLeft": 40,
		"autoMarginOffset": 20,
		"mouseWheelZoomEnabled":true,
		"dataDateFormat": "YYYY-MM-DD",
		"valueAxes": [{
			"id": "v1",
			"axisAlpha": 0,
			"position": "left",
			"ignoreAxisWidth":true,
			"precision": 0
		}],
		"balloon": {
			"borderThickness": 1,
			"shadowAlpha": 0
		},
		"graphs": [{
			"id": "g1",
			"balloon":{
			  "drop":true,
			  "adjustBorderColor":false,
			  "color":"#ffffff"
			},
			"bullet": "round",
			"bulletBorderAlpha": 1,
			"bulletColor": "#FFFFFF",
			"bulletSize": 5,
			"hideBulletsCount": 50,
			"lineThickness": 2,
			"title": "red line",
			"useLineColorForBulletBorder": true,
			"valueField": "value",
			"balloonText": "<span style='font-size:18px;'>[[value]]</span>"
		}],
		"chartScrollbar": {
			"graph": "g1",
			"oppositeAxis":false,
			"offset":30,
			"scrollbarHeight": 80,
			"backgroundAlpha": 0,
			"selectedBackgroundAlpha": 0.1,
			"selectedBackgroundColor": "#888888",
			"graphFillAlpha": 0,
			"graphLineAlpha": 0.5,
			"selectedGraphFillAlpha": 0,
			"selectedGraphLineAlpha": 1,
			"autoGridCount":true,
			"color":"#AAAAAA"
		},
		"chartCursor": {
			"pan": true,
			"valueLineEnabled": true,
			"valueLineBalloonEnabled": true,
			"cursorAlpha":1,
			"cursorColor":"#258cbb",
			"limitToGraph":"g1",
			"valueLineAlpha":0.2,
			"valueZoomable":true
		},
		"valueScrollbar":{
		  "oppositeAxis":false,
		  "offset":50,
		  "scrollbarHeight":10
		},
		"categoryField": "date",
		"categoryAxis": {
			"parseDates": true,
			"dashLength": 1,
			"minorGridEnabled": true
		},
		"export": {
			"enabled": true
		},
		"dataProvider": {!! json_encode($datedAjustes) !!}
	});
	AmCharts.makeChart("chart_finalizadas", {
		"type": "serial",
		"theme": "light",
		"marginRight": 40,
		"marginLeft": 40,
		"autoMarginOffset": 20,
		"mouseWheelZoomEnabled":true,
		"dataDateFormat": "YYYY-MM-DD",
		"valueAxes": [{
			"id": "v1",
			"axisAlpha": 0,
			"position": "left",
			"ignoreAxisWidth":true,
			"precision": 0
		}],
		"balloon": {
			"borderThickness": 1,
			"shadowAlpha": 0
		},
		"graphs": [{
			"id": "g1",
			"balloon":{
			  "drop":true,
			  "adjustBorderColor":false,
			  "color":"#ffffff"
			},
			"bullet": "round",
			"bulletBorderAlpha": 1,
			"bulletColor": "#FFFFFF",
			"bulletSize": 5,
			"hideBulletsCount": 50,
			"lineThickness": 2,
			"title": "red line",
			"useLineColorForBulletBorder": true,
			"valueField": "value",
			"balloonText": "<span style='font-size:18px;'>[[value]]</span>"
		}],
		"chartScrollbar": {
			"graph": "g1",
			"oppositeAxis":false,
			"offset":30,
			"scrollbarHeight": 80,
			"backgroundAlpha": 0,
			"selectedBackgroundAlpha": 0.1,
			"selectedBackgroundColor": "#888888",
			"graphFillAlpha": 0,
			"graphLineAlpha": 0.5,
			"selectedGraphFillAlpha": 0,
			"selectedGraphLineAlpha": 1,
			"autoGridCount":true,
			"color":"#AAAAAA"
		},
		"chartCursor": {
			"pan": true,
			"valueLineEnabled": true,
			"valueLineBalloonEnabled": true,
			"cursorAlpha":1,
			"cursorColor":"#258cbb",
			"limitToGraph":"g1",
			"valueLineAlpha":0.2,
			"valueZoomable":true
		},
		"valueScrollbar":{
		  "oppositeAxis":false,
		  "offset":50,
		  "scrollbarHeight":10
		},
		"categoryField": "date",
		"categoryAxis": {
			"parseDates": true,
			"dashLength": 1,
			"minorGridEnabled": true
		},
		"export": {
			"enabled": true
		},
		"dataProvider": {!! json_encode($datedFinalizadas) !!}
	});
	
	AmCharts.makeChart("chart_6", {
		type: "pie",
		theme: "light",
		fontFamily: "Open Sans",
		color: "#888",
		dataProvider: [{
			operacion: "Venta",
			numero: {{ count($ventas) }}
		}, {
			operacion: "Renta",
			numero: {{ count($rentas) }}
		}],
		valueField: "numero",
		titleField: "operacion",
		exportConfig: {
			menuItems: [{
				icon: App.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
				format: "png"
			}]
		}
	});
});
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDgVWtTCbEbnag9SrhyE0cfZBaomCeqge8&callback=initMap" async defer></script>

@endsection