@extends('layouts.dashboard')

@section('content')
<div class="page-content">
	<!-- BEGIN PAGE HEADER-->
	<h1 class="page-title">
		Editar Ingreso
		<!--small>subheader</small-->
	</h1>
	<!-- END PAGE HEADER-->
	<input type="hidden" id="idIngreso" value="{{ $ingreso->id }}" />
	<form id="forma" action="{{ Gate::check('update_orders')?action('IngresosController@update', $ingreso->id):'' }}" class="form-horizontal" method="post" enctype="multipart/form-data">
		{{ method_field('PUT') }}
		{{ csrf_field() }}
		<div class="row">
			<div class="col-xs-12">
				<div class="portlet light">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-bubble font-dark hide"></i>
							<span class="caption-subject font-hide bold uppercase">Información del Ingreso</span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-xs-12 col-md-6">
								<div class="row">
									<div class="form-group">
										<label for="" class="col-sm-3 control-label">Proveedor</label>
										<div class="col-sm-8">
											<input type="text" name="provider" class="form-control" value="{{ $ingreso->provider }}" />
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for="" class="col-sm-3 control-label">Tipo de Operación</label>
										<div class="col-sm-8">
											<select name="type" class="form-control">
												<option value="nuevo" {{ $ingreso->type == 'nuevo' ? 'active':'' }}>Nuevo</option>
												<option value="ajuste" {{ $ingreso->type == 'ajuste' ? 'active':'' }}>Ajuste</option>
												<option value="devolución" {{ $ingreso->type == 'devolución' ? 'active':'' }}>Devolución</option>
												<option value="intercambio" {{ $ingreso->type == 'intercambio' ? 'active':'' }}>Intercambio</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for="" class="col-sm-3 control-label">Fecha de Registro</label>
										<div class="col-sm-8">
											<input type="text" name="register_date" id="register_date" class="form-control date" value="{{ $ingreso->register_date }}" />
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for="" class="col-sm-3 control-label">Fecha de Entrada</label>
										<div class="col-sm-8">
											<input type="text" name="entry_date" id="entry_date" class="form-control date" value="{{ $ingreso->entry_date }}" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-md-6">
								<div class="row">
									<div class="form-group">
										<label for="" class="col-sm-3 control-label">Remisión</label>
										<div class="col-sm-8">
											<input type="text" name="remision" class="form-control" value="{{ $ingreso->remision }}" />
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for="" class="col-sm-3 control-label">Factura</label>
										<div class="col-sm-8">
											<input type="file" name="file" class="form-control" />
											@if($ingreso->factura != "")
											<a href="/ingresos/{{ $ingreso->factura }}" target="_blank">Ver Factura</a>
											@endif
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label for="" class="col-sm-3 control-label">Comentarios</label>
										<div class="col-sm-8">
											<textarea name="comments" class="form-control">{{ $ingreso->comentarios }}</textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="portlet light">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-bubble font-dark hide"></i>
							<span class="caption-subject font-hide bold uppercase">Lista de Productos</span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-sm-4">
								<div class="status-box" style="width:100%">Producto:</div>
								<select id="product" class="chosen">
									@foreach($products as $prod)
									<option value="{{ $prod->id }}">{{ $prod->label }}</option>
									@endforeach
								</select>
							</div>
							<div class="col-sm-5">
								<div class="row">
									<div class="col-xs-2" style="padding-right:0px;">
										<div class="status-box green" data-value="1" data-toggle="tooltip" title="Listo para Lavarse"></div>
										<input type="text" class="form-control" id="inventario_verde" value="0" style="padding:5px;" />
									</div>
									<div class="col-xs-2" style="padding-right:0px;">
										<div class="status-box blue" data-value="2" data-toggle="tooltip" title="Requiere Soldadura"></div>
										<input type="text" class="form-control" id="inventario_azul" value="0" style="padding:5px;" />
									</div>
									<div class="col-xs-2" style="padding-right:0px;">
										<div class="status-box yellow" data-value="3" data-toggle="tooltip" title="Refacciones Grandes"></div>
										<input type="text" class="form-control" id="inventario_amarillo" value="0" style="padding:5px;" />
									</div>
									<div class="col-xs-2" style="padding-right:0px;">
										<div class="status-box orange" data-value="3" data-toggle="tooltip" title="Refacciones Grandes"></div>
										<input type="text" class="form-control" id="inventario_naranja" value="0" style="padding:5px;" />
									</div>
									<div class="col-xs-2" style="padding-right:0px;">
										<div class="status-box red" data-value="4" data-toggle="tooltip" title="No es Apto"></div>
										<input type="text" class="form-control" id="inventario_rojo" value="0" style="padding:5px;" />
									</div>
									<div class="col-xs-2" style="padding-right:0px;">
										<div class="status-box" data-value="4" data-toggle="tooltip" title="Devueltos" style="width:100%;text-align:center;"><span class="fa fa-refresh" style="font-size:32px;"></span></div>
										<input type="text" class="form-control" id="devueltas" value="0" style="padding:5px;" />
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="status-box" style="width:100%">Comentarios:</div>
								<input type="text" id="products_comments" class="form-control" />
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<span class="btn btn-warning" onclick="addProduct()">Agregar</span>
							</div>
						</div>
						<div class="row" style="margin-top:25px;">
							<div class="col-sm-12">
								<table id="products_list" class="table table-stripped">
									<thead>
										<tr>
											<th>Producto</th>
											<th colspan="6">Cantidades</th>
											<th>Comentarios</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
									@foreach($ingreso->products as $i => $ingresoProd)
									<tr data-index="{{ $i }}">
										<td>{{ $ingresoProd->name }}<input type="hidden" class="product" name="products[{{ $i }}][product_id]" value="{{ $ingresoProd->id }}" /></td>
										<td class="col-xs-1" data-toggle="tooltips" title="Listo para Lavarse">{{ $ingresoProd->pivot->quantity_verde }}<input type="hidden" class="qtyverde" name="products[{{ $i }}][quantity_verde]" value="{{ $ingresoProd->pivot->quantity_verde }}" /></td>
										<td class="col-xs-1" data-toggle="tooltips" title="Requiere Soldadura">{{ $ingresoProd->pivot->quantity_azul }}<input type="hidden" class="qtyazul" name="products[{{ $i }}][quantity_azul]" value="{{ $ingresoProd->pivot->quantity_azul }}" /></td>
										<td class="col-xs-1" data-toggle="tooltips" title="Refacciones Grandes">{{ $ingresoProd->pivot->quantity_amarillo }}<input type="hidden" class="qtyamarillo" name="products[{{ $i }}][quantity_amarillo]" value="{{ $ingresoProd->pivot->quantity_amarillo }}" /></td>
										<td class="col-xs-1" data-toggle="tooltips" title="Refacciones Grandes">{{ $ingresoProd->pivot->quantity_naranja }}<input type="hidden" class="qtynaranja" name="products[{{ $i }}][quantity_naranja]" value="{{ $ingresoProd->pivot->quantity_naranja }}" /></td>
										<td class="col-xs-1" data-toggle="tooltips" title="No es Apto">{{ $ingresoProd->pivot->quantity_rojo }}<input type="hidden" class="qtyrojo" name="products[{{ $i }}][quantity_rojo]" value="{{ $ingresoProd->pivot->quantity_rojo }}" /></td>
										<td class="col-xs-1" data-toggle="tooltips" title="Devueltos">{{ $ingresoProd->pivot->devueltos }}<input type="hidden" class="qtydevueltos" name="products[{{ $i }}][quantity_devueltos]" value="{{ $ingresoProd->pivot->devueltos }}" /></td>
										<td>{{ $ingresoProd->pivot->comments }}<input type="hidden" name="products[{{ $i }}][comments]" value="{{ $ingresoProd->pivot->comments }}" /></td>
										<td><input type="button" class="btn btn-danger delete-prod" value="Elminar" /></td>
									</tr>
									@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				@can('crud_orders')
				<input type="submit" class="btn btn-success" value="Guardar" style="width:100%;" />
				@endcan
			</div>
		</div>
	</form>
</div>

<script>
var products = {!! json_encode($products) !!};
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip(); 
	$('.chosen').chosen(); 
	
	$(".date").datepicker({
		format:'yyyy-mm-dd'
	});
	
	$("#forma").submit(function(e){
		var msg = "";
		var forma = $(this);
		if(forma.find("[name=provider]").val() == ""){
			msg += "- Ingresar Proveedor\n";
		}
		if(forma.find("[name=register_date]").val() == ""){
			msg += "- Ingresar Fecha de Registro\n";
		}
		if(forma.find("[name=entry_date]").val() == ""){
			msg += "- Ingresar Fecha de Ingreso\n";
		}
		if($("input[type=hidden].product").length == 0){
			msg += "- Agregar al menos un producto\n";
		}
		
		if(msg != ""){
			alert(msg);
			e.preventDefault();
			return false;
		}
	});
	
	$.ajaxSetup({
		headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
	});
	
	$('#typeahead-products').typeahead(
		{
		  hint: true,
		  highlight: true,
		  minLength: 1
		},
		{
			source: function(query, process) {
				objects = [];
				map = {};
				
				$.each(products, function(i, object) {
					map[object.label] = object;
					objects.push(object.label);
				}); 
				console.log("objs", objects)
				process(objects);
			}
		}
	).on('typeahead:selected', function(evt, item) { 
		selectedProduct = map[item].id;
		$("#typeahead-buffer").val(selectedProduct);
	});
});
$(document).on('click', 'input.delete-prod', function(e){
	$(this).closest("tr").remove();
});
function addProduct(){
	var label = $("#product option:selected").text();
	var id  = $("#product").val();
	var qty_verde  = $("#inventario_verde").val();
	var qty_azul  = $("#inventario_azul").val();
	var qty_amarillo  = $("#inventario_amarillo").val();
	var qty_naranja  = $("#inventario_naranja").val();
	var qty_rojo  = $("#inventario_rojo").val();
	var qty_devueltos  = $("#devueltas").val();
	var comments  = $("#products_comments").val();
	var index = $("#products_list tbody tr").length == 0 ? 0 : parseInt($("#products_list tbody tr").last().attr("data-index")) + 1;
	
	var inputs = $("input.product[value="+id+"]");
	
	var html = '';
	if(inputs.length == 0){
		html += '<tr data-index="'+index+'">';
		html += '<td>'+label+'<input type="hidden" class="product" name="products['+index+'][product_id]" value="'+id+'" /></td>';
		html += '<td class="col-xs-1" data-toggle="tooltip" title="Listo para Lavarse">'+qty_verde+'<input type="hidden" class="qtyverde" name="products['+index+'][quantity_verde]" value="'+qty_verde+'" /></td>';
		html += '<td class="col-xs-1" data-toggle="tooltip" title="Requiere Soldadura">'+qty_azul+'<input type="hidden" class="qtyazul" name="products['+index+'][quantity_azul]" value="'+qty_azul+'" /></td>';
		html += '<td class="col-xs-1" data-toggle="tooltip" title="Refacciones Grandes">'+qty_amarillo+'<input type="hidden" class="qtyamarillo" name="products['+index+'][quantity_amarillo]" value="'+qty_amarillo+'" /></td>';
		html += '<td class="col-xs-1" data-toggle="tooltip" title="Refacciones Grandes">'+qty_naranja+'<input type="hidden" class="qtynaranja" name="products['+index+'][quantity_naranja]" value="'+qty_naranja+'" /></td>';
		html += '<td class="col-xs-1" data-toggle="tooltip" title="No es Apto">'+qty_rojo+'<input type="hidden" class="qtyrojo" name="products['+index+'][quantity_rojo]" value="'+qty_rojo+'" /></td>';
		html += '<td class="col-xs-1" data-toggle="tooltip" title="Devueltos">'+qty_devueltos+'<input type="hidden" class="qtydevueltos" name="products['+index+'][quantity_devueltos]" value="'+qty_devueltos+'" /></td>';
		html += '<td>'+comments+'<input type="hidden" name="products['+index+'][comments]" value="'+comments+'" /></td>';
		html += '<td><input type="button" class="btn btn-danger delete-prod" value="Elminar" /></td>';
		html += '</tr>';
		$("#products_list tbody").append(html);
	}else{
		var row = inputs.closest("tr");
		var old_verde = row.find("input.qtyverde");
		var old_azul = row.find("input.qtyazul");
		var old_amarillo = row.find("input.qtyamarillo");
		var old_naranja = row.find("input.qtynaranja");
		var old_rojo = row.find("input.qtyrojo");
		var old_devueltos = row.find("input.qtydevueltos");
		
		html += '<tr data-index="'+index+'">';
		html += '<td>'+label+'<input type="hidden" class="product" name="products['+index+'][product_id]" value="'+id+'" /></td>';
		html += '<td class="col-xs-1" data-toggle="tooltip" title="Listo para Lavarse">'+(parseInt(qty_verde) + parseInt(old_verde.val()))+'<input type="hidden" class="qtyverde" name="products['+index+'][quantity_verde]" value="'+(parseInt(qty_verde) + parseInt(old_verde.val()))+'" /></td>';
		html += '<td class="col-xs-1" data-toggle="tooltip" title="Requiere Soldadura">'+(parseInt(qty_azul) + parseInt(old_azul.val()))+'<input type="hidden" class="qtyazul" name="products['+index+'][quantity_azul]" value="'+(parseInt(qty_azul) + parseInt(old_azul.val()))+'" /></td>';
		html += '<td class="col-xs-1" data-toggle="tooltip" title="Refacciones Grandes">'+(parseInt(qty_amarillo) + parseInt(old_amarillo.val()))+'<input type="hidden" class="qtyamarillo" name="products['+index+'][quantity_amarillo]" value="'+(parseInt(qty_amarillo) + parseInt(old_amarillo.val()))+'" /></td>';
		html += '<td class="col-xs-1" data-toggle="tooltip" title="Refacciones Grandes">'+(parseInt(qty_naranja) + parseInt(old_naranja.val()))+'<input type="hidden" class="qtynaranja" name="products['+index+'][quantity_naranja]" value="'+(parseInt(qty_naranja) + parseInt(old_naranja.val()))+'" /></td>';
		html += '<td class="col-xs-1" data-toggle="tooltip" title="No es Apto">'+(parseInt(qty_rojo) + parseInt(old_rojo.val()))+'<input type="hidden" class="qtyrojo" name="products['+index+'][quantity_rojo]" value="'+(parseInt(qty_rojo) + parseInt(old_rojo.val()))+'" /></td>';
		html += '<td class="col-xs-1" data-toggle="tooltip" title="Devueltos">'+(parseInt(qty_devueltos) + parseInt(old_devueltos.val()))+'<input type="hidden" class="qtydevueltos" name="products['+index+'][quantity_devueltos]" value="'+(parseInt(qty_devueltos) + parseInt(old_devueltos.val()))+'" /></td>';
		html += '<td>'+comments+'<input type="hidden" name="products['+index+'][comments]" value="'+comments+'" /></td>';
		html += '<td><input type="button" class="btn btn-danger delete-prod" value="Elminar" /></td>';
		html += '</tr>';
		row.replaceWith(html);
	}
	
	$("input#typeahead-products").val("");
	$("#quantity").val("1");
	
	clearProductForm();
}
function clearProductForm(){
	$("#inventario_verde").val(0);
	$("#inventario_azul").val(0);
	$("#inventario_amarillo").val(0);
	$("#inventario_naranja").val(0);
	$("#inventario_rojo").val(0);
	$("#devueltas").val(0);
	$("#products_comments").val("");
}
</script>

@endsection