@extends('layouts.dashboard')

@section('content')
<div class="page-content">
	<!-- BEGIN PAGE HEADER-->
	<h1 class="page-title"> {{ ucfirst($sidemenu) }}
		<!--small>subheader</small-->
	</h1>
	<div class="page-bar">
		<ul class="page-breadcrumb">
			<li>
				<i class="icon-home"></i>
				<a href="index.html">Home</a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<span>Ingresos</span>
			</li>
		</ul>
	</div>
	<!-- END PAGE HEADER-->
	@if(session('success'))
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				{{ session('success') }}
			</div>
		</div>
	</div>
	@endif
	@if(session('error'))
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				{{ session('error') }}
			</div>
		</div>
	</div>
	@endif
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="portlet light ">
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-12">
							<table class="table table-hover table-light">
								<thead>
									<tr>
										<th>ID</th>
										<th>Fecha de Registro</th>
										<th>Fecha de Entrada</th>
										<th>Proveedor</th>
										<th># de Productos</th>
										<th>Remisión</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
								@foreach($ingresos as $ingreso)
									<tr>
										<td>{{ $ingreso->id }}</td>
										<td>{{ $ingreso->register_date }}</td>
										<td>{{ $ingreso->entry_date }}</td>
										<td>{{ $ingreso->provider }}</td>
										<td>0</td>
										<td>{{ $ingreso->remision }}</td>
										<td>
											<!--form action="{{ Gate::check('delete_orders')?action('OrdersController@destroy', [$ingreso->id]):'' }}" method="post"-->
											<form action="{{ action('IngresosController@destroy', [$ingreso->id]) }}" method="post">
												<input name="_method" type="hidden" value="DELETE">
												{{ csrf_field() }}
												<a href="{{ action('IngresosController@edit', $ingreso->id) }}" class="btn btn-xs btn-primary" data-toggle="tooltip" title="Editar Ingreso"><i class="fa fa-pencil" aria-hidden="true"></i></a>
												{{-- @can('delete_orders') --}}
												<button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Eliminar Ingreso"><i class="fa fa-times" aria-hidden="true"></i></button>
												{{-- @endcan --}}
											</form>
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<a href="{{ action('IngresosController@create') }}" class="btn green"> Crear
								<i class="fa fa-plus"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip({html: true}); 
});
</script>

@endsection