@extends('layouts.dashboard')

@section('content')
<div class="page-content">
	<!-- BEGIN PAGE HEADER-->
	<h1 class="page-title">
		Nueva Orden
		<!--small>subheader</small-->
	</h1>
	<!-- END PAGE HEADER-->
	<form id="forma" action="{{ action('OrdersController@store') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<div class="portlet light">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-bubble font-dark hide"></i>
							<span class="caption-subject font-hide bold uppercase">Información de la Orden</span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Cliente</label>
								<div class="col-sm-8">
									<input type="text" name="client" class="form-control" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Tipo de Operación</label>
								<div class="col-sm-8">
									<select name="type" class="form-control" onchange="showEndDate(this);">
										<option value="renta">Renta</option>
										<option value="venta">Venta</option>
										<option value="muestra">Muestra</option>
										<option value="prestamo">Préstamo</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Fecha de Inicio</label>
								<div class="col-sm-8">
									<input type="text" name="start_date" id="start_date" class="form-control date" value="{{ (new Datetime())->format('Y-m-d') }}" />
								</div>
							</div>
						</div>
						<div class="row end_date">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Fecha de Fin</label>
								<div class="col-sm-8">
									<input type="text" name="end_date" class="form-control date" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Número de Carta Porte</label>
								<div class="col-sm-8">
									<input type="text" name="numero_cartaporte" class="form-control" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Carta Porte</label>
								<div class="col-sm-8">
									<input type="file" name="file" class="form-control" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Comentarios</label>
								<div class="col-sm-8">
									<textarea name="comments" class="form-control"></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Foto</label>
								<div class="col-sm-8">
									<input type="file" name="photo" class="form-control" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="portlet light">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-bubble font-dark hide"></i>
							<span class="caption-subject font-hide bold uppercase">Lista de Productos</span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-sm-6">
								<select id="product" class="chosen">
									@foreach($products as $prod)
									<option value="{{ $prod->id }}">{{ $prod->name }} - {{ $prod->color->name }}</option>
									@endforeach
								</select>
							</div>
							<div class="col-sm-4">
								<input type="number" id="quantity" class="form-control" placeholder="Cantidad" value="1" />
							</div>
							<div class="col-sm-2">
								<input type="button" class="btn btn-primary" value="Agregar" onclick="addProduct();" />
							</div>
						</div>
						<div class="row" style="margin-top:25px;">
							<div class="col-sm-12">
								<table id="products_list" class="table table-stripped">
									<thead>
										<tr>
											<th>Producto</th>
											<th>Cantidad</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<label for="" class="control-label">Tiempo Estimado (días)</label>
								<div class="row">
									<div class="col-sm-8">
										<input type="text" name="estimated_time" id="tiempo_estimado" class="form-control" />
									</div>
									<div class="col-sm-4">
										<span class="btn btn-primary" onclick="getEstimate()">Calcular</span>
									</div>
								</div>
							</div>
						</div>						
						<div class="row">
							<div class="col-sm-12">
								<label for="" class="control-label">Necesita Modificaciones</label>
								<select name="ajustes" class="form-control" onchange="showAjustes(this);">
									<option value="0">No</option>
									<option value="1">Si</option>
								</select>
							</div>
						</div>
						<div class="row ajustes_comments hidden">
							<div class="col-sm-12">
								<label for="" class="control-label">¿Qué modificaciones se necesitan?</label>
								<textarea name="ajustes_comments" class="form-control" placeholder="" style="height:150px;resize:none;"></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-md-12">
				<input type="submit" class="btn btn-success" value="Guardar" style="width:100%;" />
			</div>
		</div>
	</form>
</div>

<script>
var products = {!! json_encode($products) !!};
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip(); 
	$('.chosen').chosen(); 
	
	$(".date").datepicker({
		format:'yyyy-mm-dd'
	});
	
	$("#forma").submit(function(e){
		var msg = "";
		var forma = $(this);
		if(forma.find("[name=client]").val() == ""){
			msg += "- Ingresar Cliente\n";
		}
		if(forma.find("[name=start_date]").val() == ""){
			msg += "- Ingresar Fecha de Inicio\n";
		}
		if(forma.find("[name=numero_cartaporte]").val() == ""){
			msg += "- Ingresar folio de carta porte\n";
		}
		if(forma.find("[name=file]").val() == ""){
			msg += "- Adjuntar Carta Porte\n";
		}
		if(forma.find("[name=type]").val() == "renta"){
			if(forma.find("[name=end_date]").val() == ""){
				msg += "- Ingresar Fecha de Fin\n";
			}
		}
		if($("input[type=hidden].product").length == 0){
			msg += "- Agregar al menos un producto\n";
		}
		
		if(msg != ""){
			alert(msg);
			e.preventDefault();
			return false;
		}
	});
	
	$.ajaxSetup({
		headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
	});
	
	$('#typeahead-products').typeahead(
		{
		  hint: true,
		  highlight: true,
		  minLength: 1
		},
		{
			source: function(query, process) {
				objects = [];
				map = {};
				
				$.each(products, function(i, object) {
					map[object.label] = object;
					objects.push(object.label);
				}); 
				console.log("objs", objects)
				process(objects);
			}
		}
	).on('typeahead:selected', function(evt, item) { 
		selectedProduct = map[item].id;
		$("#typeahead-buffer").val(selectedProduct);
	});
});
$(document).on('click', 'input.delete-prod', function(e){
	$(this).closest("tr").remove();
});
function addProduct(){
	var label = $("#product option:selected").text();
	var id  = $("#product").val();
	var qty  = $("#quantity").val();
	var index = $("#products_list tbody tr").length == 0 ? 0 : parseInt($("#products_list tbody tr").last().attr("data-index")) + 1;
	
	var inputs = $("input.product[value="+id+"]");
	
	var html = '';
	if(inputs.length == 0){
		html += '<tr data-index="'+index+'">';
		html += '<td>'+label+'<input type="hidden" class="product" name="products['+index+'][product_id]" value="'+id+'" /></td>';
		html += '<td>'+qty+'<input type="hidden" class="qty" name="products['+index+'][quantity]" value="'+qty+'" /></td>';
		html += '<td><input type="button" class="btn btn-danger delete-prod" value="Elminar" /></td>';
		html += '</tr>';
		$("#products_list tbody").append(html);
	}else{
		var row = inputs.closest("tr");
		var old = row.find("input.qty");
		html += '<tr data-index="'+index+'">';
		html += '<td>'+label+'<input type="hidden" class="product" name="products['+index+'][product_id]" value="'+id+'" /></td>';
		html += '<td>'+(parseInt(qty) + parseInt(old.val()))+'<input type="hidden" class="qty" name="products['+index+'][quantity]" value="'+(parseInt(qty) + parseInt(old.val()))+'" /></td>';
		html += '<td><input type="button" class="btn btn-danger delete-prod" value="Elminar" /></td>';
		html += '</tr>';
		row.replaceWith(html);
	}
	
	$("input#typeahead-products").val("");
	$("#quantity").val("1");
}
function showEndDate(elm){
	var val = $(elm).val();
	if(val == "venta")	$(".end_date").addClass("hide");
	else				$(".end_date").removeClass("hide");
}
function showAjustes(elm){
	var val = $(elm).val();
	if(val == "1")	$(".ajustes_comments").removeClass("hidden");
	else				$(".ajustes_comments").addClass("hidden");
}
function getEstimate(){
	var start_date = $("#start_date").val();
	products = $.map($("input[type=hidden].product"), function(val, i){
		return $(val).val();
	});
	cantidades = $.map($("input[type=hidden].qty"), function(val, i){
		return $(val).val();
	});
	
	$.ajax({
		url: '/orders/estimate',
		type: 'POST',
		data: {
			'start_date' : start_date,
			'products' : products,
			'cantidades' : cantidades
		},
		dataType: 'json'
	})
	.done(function(response) {
		console.log("success");
		if(response > 0)
			$("#tiempo_estimado").val(response);
		else
			alert("No hay disponibilidad.");
	})
	.fail(function(response) {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}
</script>

@endsection