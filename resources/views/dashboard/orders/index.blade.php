@extends('layouts.dashboard')

@section('content')
<div class="page-content">
	<!-- BEGIN PAGE HEADER-->
	<h1 class="page-title"> {{ ucfirst($sidemenu) }}
		<!--small>subheader</small-->
	</h1>
	<div class="page-bar">
		<ul class="page-breadcrumb">
			<li>
				<i class="icon-home"></i>
				<a href="index.html">Home</a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<span>Ordenes</span>
			</li>
		</ul>
	</div>
	<!-- END PAGE HEADER-->
	@if(session('success'))
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				{{ session('success') }}
			</div>
		</div>
	</div>
	@endif
	@if(session('error'))
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				{{ session('error') }}
			</div>
		</div>
	</div>
	@endif
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="portlet light ">
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-12">
							<table class="table table-hover table-light">
								<thead>
									<tr>
										<th></th>
										<th>ID</th>
										<th>Fecha</th>
										<th>Cliente</th>
										<th># de Carta Porte</th>
										<th>Tipo</th>
										<th># de Productos</th>
										<th>¿Necesita Modificaciones?</th>
										<th>Días estimados</th>
										<th>Unidades Faltantes</th>
										<th>Status</th>
										<th>Comentarios</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
								@foreach($orders as $order)
									<tr>
										<td><input type="checkbox" name="massive" value="{{ $order->id }}" /></td>
										<td>{{ $order->id }}</td>
										<td>{{ $order->start_date }}</td>
										<td>{{ $order->client }}</td>
										<td>{{ $order->numero_cartaporte }}</td>
										<td>{{ ucfirst($order->type) }}</td>
										<td>{{ $order->total_products() }}</td>
										<td>{{ $order->ajustes ? 'Si':'No' }}</td>
										<td>{{ $order->estimated_time > -1 ? $order->estimated_time:'No hay disponibilidad' }}</td>
										<td class="faltantes">
											<?php
												$count = 0;
												$tooltip = "";
												//$separados = $product->availability($today);
												//echo IS_NULL($separados[0]->separados) ? 0 : $separados[0]->separados;
											?>
											@if($order->status == 1)
											<span class="label label-sm label-green">Finalizada</span>
											@elseif($order->separados == 1)
											<span class="label label-sm label-primary">Pendiente de Entregar</span>
											@else
												@foreach($order->products as $product)
													@if($product->availability($order->start_date) < $product->pivot->quantity)
														<?php 
															$tooltip .= "- ".$product->name.": ".($product->pivot->quantity - $product->availability($order->start_date))."<br /";
															$count += $product->pivot->quantity - $product->availability($order->start_date); 
														?>
													@endif
												@endforeach
												
												@if($count == 0)
												<a href="{{ Gate::check('separar_orders')?action('OrdersController@separar', $order->id):'' }}" class="label label-sm label-warning">Separar</a>
												@else
												<span class="label label-sm label-danger" data-toggle="tooltip" title="{{ $tooltip }}"><strong>{{ $count }}</strong></span>
												@endif
											@endif
										</td>
										<td>
											@if($order->status == 0)
											<a href="{{ Gate::check('finalizar_orders')?action('OrdersController@liberar', $order->id):'' }}" class="label label-sm label-danger">Finalizar</a>
											@elseif($order->status == 1)
											<span class="label label-sm label-green">Entregado</span>
											@endif
										</td>
										<td>{{ $order->comments }}</td>
										<td>
											<form action="{{ Gate::check('delete_orders')?action('OrdersController@destroy', [$order->id]):'' }}" method="post">
												<input name="_method" type="hidden" value="DELETE">
												{{ csrf_field() }}
												<a href="/orders/{{ $order->id }}/edit" class="btn btn-xs btn-primary" data-toggle="tooltip" title="Editar Orden"><i class="fa fa-pencil" aria-hidden="true"></i></a>
												@can('delete_orders')
												<button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Eliminar Orden"><i class="fa fa-times" aria-hidden="true"></i></button>
												@endcan
											</form>
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<span class="btn btn-warning" onclick="sendMassive('separar')" style="margin-left:30px;">Separar</span>
							<span class="btn btn-danger" onclick="sendMassive('finalizar')">Finalizar</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<form id="massive" action="{{ action('OrdersController@massive') }}" method="post">
	{{ csrf_field() }}
	<input type="hidden" name="action" />
</form>

<script>
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip({html: true}); 
});

function sendMassive(action){
	$("#massive [name=action]").val(action);
	$("#massive [name='ids[]']").remove();
	var selected = $("input[type=checkbox][name=massive]:checked");
	
	if(selected.length > 0){
		$.each(selected, function(i, val){
			$("#massive").append('<input type="hidden" name="ids[]" value="'+$(val).val()+'" />');
		});
		
		$("form#massive").submit();
	} else {
		alert("Selecciona al menos una orden.");
	}
}
</script>

@endsection