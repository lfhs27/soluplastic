@extends('layouts.dashboard')

@section('content')
<div class="page-content">
	<!-- BEGIN PAGE HEADER-->
	<h1 class="page-title">
		Nuevo Producto
		<!--small>subheader</small-->
	</h1>
	<!-- END PAGE HEADER-->
	<form action="{{ action('ProductsController@store') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<div class="portlet light">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-bubble font-dark hide"></i>
							<span class="caption-subject font-hide bold uppercase">Información del Producto</span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Nombre</label>
								<div class="col-sm-8">
									<input type="text" name="name" class="form-control" id="" value="">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Tipo de Producto</label>
								<div class="col-sm-8">
									<select name="product_type_id" class="form-control">
										@foreach($productTypes as $type)
										<option value="{{ $type->id }}">{{ $type->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Marca</label>
								<div class="col-sm-8">
									<select name="brand_id" class="form-control">
										@foreach($brands as $brand)
										<option value="{{ $brand->id }}">{{ $brand->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Color</label>
								<div class="col-sm-8">
									<select name="color_id" class="form-control">
										@foreach($colors as $color)
										<option value="{{ $color->id }}">{{ $color->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Tipo de Base</label>
								<div class="col-sm-8">
									<select name="base_id" class="form-control">
										@foreach($bases as $base)
										<option value="{{ $base->id }}">{{ $base->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Tipo de Puerta</label>
								<div class="col-sm-8">
									<select name="door_id" class="form-control">
										@foreach($doors as $door)
										<option value="{{ $door->id }}">{{ $door->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Categoría</label>
								<div class="col-sm-8">
									<select name="category_id" class="form-control">
										@foreach($categories as $category)
										<option value="{{ $category->id }}">{{ $category->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Clasificación</label>
								<div class="col-sm-8">
									<select name="classification_id" class="form-control">
										@foreach($classifications as $classification)
										<option value="{{ $classification->id }}">{{ $classification->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Status</label>
								<div class="col-sm-8">
									<input type="hidden" name="status" value="1" />
									<div class="row">
										<div class="col-xs-2" style="padding-right:0px;">
											<div class="status-box green" data-value="1" data-toggle="tooltip" title="Listo para Lavarse"></div>
											<input type="text" class="form-control" name="inventario_verde" value="0" style="padding:5px;" />
										</div>
										<div class="col-xs-2" style="padding-right:0px;">
											<div class="status-box blue" data-value="2" data-toggle="tooltip" title="Requiere Soldadura"></div>
											<input type="text" class="form-control" name="inventario_azul" value="0" style="padding:5px;" />
										</div>
										<div class="col-xs-2" style="padding-right:0px;">
											<div class="status-box yellow" data-value="3" data-toggle="tooltip" title="Refacciones Grandes"></div>
											<input type="text" class="form-control" name="inventario_amarillo" value="0" style="padding:5px;" />
										</div>
										<div class="col-xs-2" style="padding-right:0px;">
											<div class="status-box orange" data-value="3" data-toggle="tooltip" title="Refacciones Grandes"></div>
											<input type="text" class="form-control" name="inventario_naranja" value="0" style="padding:5px;" />
										</div>
										<div class="col-xs-2" style="padding-right:0px;">
											<div class="status-box red" data-value="4" data-toggle="tooltip" title="No es Apto"></div>
											<input type="text" class="form-control" name="inventario_rojo" value="0" style="padding:5px;" />
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Tiempo (<100)</label>
								<div class="col-sm-8">
									<input type="hidden" name="status" value="1" />
									<div class="row">
										<div class="col-xs-2" style="padding-right:0px;">
											<input type="text" class="form-control" name="tiempo_verde_baja" value="1" style="padding:5px;" />
										</div>
										<div class="col-xs-2" style="padding-right:0px;">
											<input type="text" class="form-control" name="tiempo_azul_baja" value="1" style="padding:5px;" />
										</div>
										<div class="col-xs-2" style="padding-right:0px;">
											<input type="text" class="form-control" name="tiempo_amarillo_baja" value="1" style="padding:5px;" />
										</div>
										<div class="col-xs-2" style="padding-right:0px;">
											<input type="text" class="form-control" name="tiempo_naranja_baja" value="1" style="padding:5px;" />
										</div>
										<div class="col-xs-2" style="padding-right:0px;"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Tiempo (>100)</label>
								<div class="col-sm-8">
									<input type="hidden" name="status" value="1" />
									<div class="row">
										<div class="col-xs-2" style="padding-right:0px;">
											<input type="text" class="form-control" name="tiempo_verde_alta" value="1" style="padding:5px;" />
										</div>
										<div class="col-xs-2" style="padding-right:0px;">
											<input type="text" class="form-control" name="tiempo_azul_alta" value="1" style="padding:5px;" />
										</div>
										<div class="col-xs-2" style="padding-right:0px;">
											<input type="text" class="form-control" name="tiempo_amarillo_alta" value="1" style="padding:5px;" />
										</div>
										<div class="col-xs-2" style="padding-right:0px;">
											<input type="text" class="form-control" name="tiempo_naranja_alta" value="1" style="padding:5px;" />
										</div>
										<div class="col-xs-2" style="padding-right:0px;"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Alerta de Inventario</label>
								<div class="col-sm-8">
									<input type="number" class="form-control" name="quantity_alert" value="0" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Dimensiones</label>
								<div class="col-sm-8">
									<input type="number" class="form-control" name="width" value="0" style="display:inline-block; width:70px;" placeholder="Ancho" data-toggle="tooltip" title="Ancho" />
									X
									<input type="number" class="form-control" name="length" value="0" style="display:inline-block; width:70px;" placeholder="Largo" data-toggle="tooltip" title="Largo" />
									X
									<input type="number" class="form-control" name="height" value="0" style="display:inline-block; width:70px;" placeholder="Alto" data-toggle="tooltip" title="Altura" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Imágen</label>
								<div class="col-sm-8">
									<input type="file" name="photo_file" class="form-control" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<input type="submit" class="btn btn-success" value="Guardar" style="width:100%;" />
			</div>
		</div>
	</form>
</div>

<script>
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip(); 
});
$(document).on("click", ".status-box", function(e){
	var value = $(this).attr("data-value");
	$("input[name=status]").val(value);
	
	$(".status-box").removeClass("active");
	$(this).addClass("active");
});

</script>

@endsection