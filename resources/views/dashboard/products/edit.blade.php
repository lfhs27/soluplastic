@extends('layouts.dashboard')

@section('content')
<div class="page-content">
	<!-- BEGIN PAGE HEADER-->
	<h1 class="page-title">
		Nuevo Producto
		<!--small>subheader</small-->
	</h1>
	<!-- END PAGE HEADER-->
	<form action="{{ action('ProductsController@update', $product->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
		{{ method_field('PUT') }}
		{{ csrf_field() }}
		<div class="grid row">
			<div class="grid-sizer col-xs-12 col-md-6"></div>
			<div class="grid-item col-xs-12 col-md-6">
				<div class="portlet light">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-bubble font-dark hide"></i>
							<span class="caption-subject font-hide bold uppercase">Información del Producto</span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Nombre</label>
								<div class="col-sm-8">
									<input type="text" name="name" class="form-control" id="" value="{{ $product->name }}" {{ !Gate::check('update_product')?'disabled':'' }}>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Tipo de Producto</label>
								<div class="col-sm-8">
									<select name="product_type_id" class="form-control" {{ !Gate::check('update_product')?'disabled':'' }}>
										@foreach($productTypes as $type)
										<option value="{{ $type->id }}" {{ ($product->product_type_id == $type->id ? "selected" : "") }}>{{ $type->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Marca</label>
								<div class="col-sm-8">
									<select name="brand_id" class="form-control" {{ !Gate::check('update_product')?'disabled':'' }}>
										@foreach($brands as $brand)
										<option value="{{ $brand->id }}" {{ ($product->brand_id == $brand->id ? 'selected' : '') }}>{{ $brand->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Color</label>
								<div class="col-sm-8">
									<select name="color_id" class="form-control" {{ !Gate::check('update_product')?'disabled':'' }}>
										@foreach($colors as $color)
										<option value="{{ $color->id }}" {{ ($product->color_id == $color->id ? "selected" : "") }}>{{ $color->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Tipo de Base</label>
								<div class="col-sm-8">
									<select name="base_id" class="form-control" {{ !Gate::check('update_product')?'disabled':'' }}>
										@foreach($bases as $base)
										<option value="{{ $base->id }}" {{ ($product->base_id == $base->id ? "selected" : "") }}>{{ $base->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Tipo de Puerta</label>
								<div class="col-sm-8">
									<select name="door_id" class="form-control" {{ !Gate::check('update_product')?'disabled':'' }}>
										@foreach($doors as $door)
										<option value="{{ $door->id }}" {{ ($product->door_id == $door->id ? "selected" : "") }}>{{ $door->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Categoría</label>
								<div class="col-sm-8">
									<select name="category_id" class="form-control" {{ !Gate::check('update_product')?'disabled':'' }}>
										@foreach($categories as $category)
										<option value="{{ $category->id }}" {{ ($product->category_id == $category->id ? "selected" : "") }}>{{ $category->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Clasificación</label>
								<div class="col-sm-8">
									<select name="classification_id" class="form-control" {{ !Gate::check('update_product')?'disabled':'' }}>
										@foreach($classifications as $classification)
										<option value="{{ $classification->id }}" {{ ($product->classification_id == $classification->id ? "selected" : "") }}>{{ $classification->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Status</label>
								<div class="col-sm-8">
									<input type="hidden" name="status" value="1" />
									<div class="row">
										<div class="col-xs-2" style="padding-right:0px;">
											<div class="status-box green {{ ($product->status == 1 ? 'active' : '') }}" data-value="1" data-toggle="tooltip" title="Listo para Lavarse"></div>
											<input type="number" class="form-control" name="inventario_verde" value="{{ $product->inventario_verde }}" style="padding:5px;" {{ !Gate::check('update_inventory')?'disabled':'' }} />
										</div>
										<div class="col-xs-2" style="padding-right:0px;">
											<div class="status-box blue {{ ($product->status == 2 ? 'active' : '') }}" data-value="2" data-toggle="tooltip" title="Requiere Soldadura"></div>
											<input type="number" class="form-control" name="inventario_azul" value="{{ $product->inventario_azul }}" style="padding:5px;" {{ !Gate::check('update_inventory')?'disabled':'' }} />
										</div>
										<div class="col-xs-2" style="padding-right:0px;">
											<div class="status-box yellow {{ ($product->status == 3 ? 'active' : '') }}" data-value="3" data-toggle="tooltip" title="Refacciones Grandes"></div>
											<input type="number" class="form-control" name="inventario_amarillo" value="{{ $product->inventario_amarillo }}" style="padding:5px;" {{ !Gate::check('update_inventory')?'disabled':'' }} />
										</div>
										<div class="col-xs-2" style="padding-right:0px;">
											<div class="status-box orange" data-value="3" data-toggle="tooltip" title="Refacciones Grandes"></div>
											<input type="text" class="form-control" name="inventario_naranja" value="{{ $product->inventario_naranja }}" style="padding:5px;" {{ !Gate::check('update_inventory')?'disabled':'' }} />
										</div>
										<div class="col-xs-2" style="padding-right:0px;">
											<div class="status-box red {{ ($product->status == 4 ? 'active' : '') }}" data-value="4" data-toggle="tooltip" title="No es Apto"></div>
											<input type="number" class="form-control" name="inventario_rojo" value="{{ $product->inventario_rojo }}" style="padding:5px;" {{ !Gate::check('update_inventory')?'disabled':'' }} />
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Tiempo (<100)</label>
								<div class="col-sm-8">
									<input type="hidden" name="status" value="1" />
									<div class="row">
										<div class="col-xs-2" style="padding-right:0px;">
											<input type="text" class="form-control" name="tiempo_verde_baja" value="{{ $product->tiempo_verde_baja }}" style="padding:5px;" {{ !Gate::check('update_product')?'disabled':'' }} />
										</div>
										<div class="col-xs-2" style="padding-right:0px;">
											<input type="text" class="form-control" name="tiempo_azul_baja" value="{{ $product->tiempo_azul_baja }}" style="padding:5px;" {{ !Gate::check('update_product')?'disabled':'' }} />
										</div>
										<div class="col-xs-2" style="padding-right:0px;">
											<input type="text" class="form-control" name="tiempo_amarillo_baja" value="{{ $product->tiempo_amarillo_baja }}" style="padding:5px;" {{ !Gate::check('update_product')?'disabled':'' }} />
										</div>
										<div class="col-xs-2" style="padding-right:0px;">
											<input type="text" class="form-control" name="tiempo_naranja_baja" value="{{ $product->tiempo_naranja_baja }}" style="padding:5px;" {{ !Gate::check('update_product')?'disabled':'' }} />
										</div>
										<div class="col-xs-2" style="padding-right:0px;"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Tiempo (>100)</label>
								<div class="col-sm-8">
									<input type="hidden" name="status" value="1" />
									<div class="row">
										<div class="col-xs-2" style="padding-right:0px;">
											<input type="text" class="form-control" name="tiempo_verde_alta" value="{{ $product->tiempo_verde_alta }}" style="padding:5px;" {{ !Gate::check('update_product')?'disabled':'' }} />
										</div>
										<div class="col-xs-2" style="padding-right:0px;">
											<input type="text" class="form-control" name="tiempo_azul_alta" value="{{ $product->tiempo_azul_alta }}" style="padding:5px;" {{ !Gate::check('update_product')?'disabled':'' }} />
										</div>
										<div class="col-xs-2" style="padding-right:0px;">
											<input type="text" class="form-control" name="tiempo_amarillo_alta" value="{{ $product->tiempo_amarillo_alta }}" style="padding:5px;" {{ !Gate::check('update_product')?'disabled':'' }} />
										</div>
										<div class="col-xs-2" style="padding-right:0px;">
											<input type="text" class="form-control" name="tiempo_naranja_alta" value="{{ $product->tiempo_naranja_alta }}" style="padding:5px;" {{ !Gate::check('update_product')?'disabled':'' }} />
										</div>
										<div class="col-xs-2" style="padding-right:0px;"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Inventario</label>
								<div class="col-sm-8">
									<input type="number" class="form-control" name="quantity" value="{{ $product->inventario_total() }}" disabled />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Alerta de Inventario</label>
								<div class="col-sm-8">
									<input type="number" class="form-control" name="quantity_alert" value="{{ $product->quantity_alert }}" {{ !Gate::check('update_product')?'disabled':'' }} />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Separado</label>
								<div class="col-sm-8">
									<?php
										$today = (new \Datetime())->format("Y-m-d");
										echo $product->separados($today);
									?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Dimensiones</label>
								<div class="col-sm-8">
									<input type="number" class="form-control" name="width" value="{{ $product->width }}" style="display:inline-block; width:70px;" placeholder="Ancho" data-toggle="tooltip" title="Ancho" {{ !Gate::check('update_product')?'disabled':'' }} />
									X
									<input type="number" class="form-control" name="length" value="{{ $product->length }}" style="display:inline-block; width:70px;" placeholder="Largo" data-toggle="tooltip" title="Largo" {{ !Gate::check('update_product')?'disabled':'' }} />
									X
									<input type="number" class="form-control" name="height" value="{{ $product->height }}" style="display:inline-block; width:70px;" placeholder="Alto" data-toggle="tooltip" title="Altura" {{ !Gate::check('update_product')?'disabled':'' }} />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="grid-item col-xs-12 col-md-6">
				<div class="portlet light">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-bubble font-dark hide"></i>
							<span class="caption-subject font-hide bold uppercase">Calendario</span>
						</div>
					</div>
					<div class="portlet-body">
						<div id="calendario"></div>
						<div class="row">
							<div class="col-xs-12 col-md-6">
								<strong>Disponibles</strong><br />
								{{ $product->availability($today) }}
							</div>
							<div class="col-xs-12 col-md-6">
								<strong>Rentados</strong><br />
								{{ $product->rentados($today) }}
							</div>
						</div>
					</div>
				</div>
			</div>
			{{--
			<div class="grid-item col-xs-12 col-md-6">
				<div class="portlet light">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-bubble font-dark hide"></i>
							<span class="caption-subject font-hide bold uppercase">Historial de Carta Portes</span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-xs-12">
								<table class="table table-hover table-light">
									<thead>
										<tr>
											<th>Fecha</th>
											<th>Liga</th>
											<th>Comprador</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										@forelse($product->carta_portes as $carta)
										<tr>
											<td>{{ $carta->fecha }}</td>
											<td><a href="/cartas/products/{{ $carta->file }}" target="_blank">Abrir</a></td>
											<td>{{ $carta->user->name }}</td>
											<td></td>
										</tr>
										@empty
										@endforelse
										<tr>
											<td><input type="text" name="carta_porte_fecha" class="form-control date" value="{{ (new Datetime())->format('Y-m-d') }}" /></td>
											<td><input type="file" name="carta_porte_file" class="form-control" /></td>
											<td>{{ Auth::user()->name }}</td>
											<td></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			--}}
			<div class="grid-item col-xs-12 col-md-6">
				<div class="portlet light">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-bubble font-dark hide"></i>
							<span class="caption-subject font-hide bold uppercase">Historial de Ingresos</span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-xs-12">
								<table class="table table-hover table-light">
									<thead>
										<tr>
											<th>Fecha Registro</th>
											<th>Fecha Entrada</th>
											<th>Cantidad</th>
											<th>Remisión</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										@forelse($product->ingresos as $ingreso)
										<tr>
											<td>{{ $ingreso->register_date }}</td>
											<td>{{ $ingreso->entry_date }}</td>
											<td>{{ 
												$ingreso->pivot->quantity_verde + 
												$ingreso->pivot->quantity_azul + 
												$ingreso->pivot->quantity_amarillo + 
												$ingreso->pivot->quantity_naranja + 
												$ingreso->pivot->quantity_rojo + 
												$ingreso->pivot->devueltos
												}}</td>
											<td>{{ $ingreso->remision }}</td>
											<td><a href="{{ action('IngresosController@edit', $ingreso->id) }}">Ver</a></td>
										</tr>
										@empty
										@endforelse
									</tbody>
								</table>
								{{--
								<table class="table table-hover table-light">
									<thead>
										<tr>
											<th>Fecha</th>
											<th>Factura</th>
											<th>Cantidad</th>
											<th>Devueltos</th>
											<th>Comentarios</th>
										</tr>
									</thead>
									<tbody>
										@forelse($product->facturas as $factura)
										<tr>
											<td>{{ $factura->fecha }}</td>
											<td><a href="/facturas/products/{{ $factura->path }}" target="_blank">{{ $factura->numero?$factura->numero:'Factura' }}</a></td>
											<td>{{ $factura->quantity }}</td>
											<td>{{ $factura->devueltos }}</td>
											<td>{{ $factura->comments }}</td>
										</tr>
										@empty
										@endforelse
										<tr>
											<td><input type="text" name="factura_fecha" class="form-control date" value="{{ (new Datetime())->format('Y-m-d') }}" /></td>
											<td>
												<input type="text" name="factura_numero" class="form-control" placeholder="Número de Factura" />
												<input type="file" name="factura_file" class="form-control" />
											</td>
											<td><input type="number" name="factura_quantity" class="form-control" value="0" /></td>
											<td><input type="number" name="factura_devueltos" class="form-control" value="0" /></td>
											<td><input type="text" name="factura_comments" class="form-control" value="" /></td>
										</tr>
									</tbody>
								</table>
								--}}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="grid-item col-xs-12 col-md-6">
				<div class="portlet light">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-bubble font-dark hide"></i>
							<span class="caption-subject font-hide bold uppercase">Historial de Movimientos</span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-xs-12">
								<table class="table table-hover table-light">
									<thead>
										<tr>
											<th>ID</th>
											<th>Fecha</th>
											<th>Cliente</th>
											<th>Tipo</th>
											<th>Cantidad</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										@forelse($product->orders as $order)
										<tr>
											<td>{{ $order->id }}</td>
											<td>{{ $order->start_date }}</td>
											<td>{{ $order->client }}</td>
											<td>{{ ucfirst($order->type) }}</td>
											<td>{{ $order->pivot->quantity }}</td>
											<td><a href="/orders/{{ $order->id }}/edit">Ver</a></td>
										</tr>
										@empty
										@endforelse
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="grid-item col-xs-12 col-md-6">
				<div class="portlet light">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-bubble font-dark hide"></i>
							<span class="caption-subject font-hide bold uppercase">Galería</span>
						</div>
					</div>
					<div class="portlet-body">
						@if(COUNT($product->photos))
						<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
							<!-- Indicators -->
							<ol class="carousel-indicators">
								@foreach($product->photos as $i => $photo)
								<li data-target="#carousel-example-generic" data-slide-to="0" class="{{ $i == 0 ? 'active':'' }}"></li>
								@endforeach
							</ol>

							<!-- Wrapper for slides -->
							<div class="carousel-inner" role="listbox">
								@foreach($product->photos as $i => $photo)
								<div class="item {{ $i == 0 ? 'active':'' }}">
									<img src="/photos/products/{{ $photo->path }}" alt="..." width="100%">
								</div>
								@endforeach
							</div>

							<!-- Controls -->
							<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
								<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
								<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
						@endif
						<div class="row">
							<div class="col-xs-12">
								<strong>Subir Imágen</strong><br />
								<input type="file" name="photo_file" class="form-control" {{ !Gate::check('add_photos')?'disabled':'' }} />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<input type="submit" class="btn btn-success" value="Guardar" style="width:100%;" />
			</div>
		</div>
	</form>
</div>

<script>
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip(); 
	
	$('#calendario').fullCalendar({
		header: "Fechass",
		defaultView: "month",
		slotMinutes: 15,
		editable: false,
		droppable: false,
		events: {!! json_encode($calendar) !!}
    });
	
	$(".date").datepicker({
		format: "yyyy-mm-dd"
	});
	
	$('.grid').masonry({
		itemSelector: '.grid-item',
		columnWidth: '.grid-sizer',
		percentPosition: true
	});
});
$(document).on("click", ".status-box", function(e){
	var value = $(this).attr("data-value");
	$("input[name=status]").val(value);
	
	$(".status-box").removeClass("active");
	$(this).addClass("active");
});

</script>

@endsection