@extends('layouts.dashboard')

@section('content')
<div class="page-content">
	<!-- BEGIN PAGE HEADER-->
	<h1 class="page-title"> {{ ucfirst($sidemenu) }}
		<!--small>subheader</small-->
	</h1>
	<div class="page-bar">
		<ul class="page-breadcrumb">
			<li>
				<i class="icon-home"></i>
				<a href="index.html">Home</a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<span>Productos</span>
			</li>
		</ul>
	</div>
	<!-- END PAGE HEADER-->
	@if(session('success'))
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				{{ session('success') }}
			</div>
		</div>
	</div>
	@endif
	@if(session('error'))
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				{{ session('error') }}
			</div>
		</div>
	</div>
	@endif
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="portlet light ">
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-12">
							<table class="table table-hover table-light">
								<thead>
									<tr>
										<th>ID</th>
										<th>Nombre</th>
										<th>Color</th>
										<th>Dimensiones</th>
										<th>Inventario</th>
										<th>Separados</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
								@foreach($products as $product)
									<tr>
										<td>{{ $product->id }}</td>
										<td>{{ $product->name }}</td>
										<td>{{ $product->color->name }}</td>
										<td>{{ $product->width }}" x {{ $product->length }}" x {{ $product->height }}"</td>
										<td>{{ $product->inventario_total() }}</td>
										<td>
										<?php
											$today = (new \Datetime())->format("Y-m-d");
											echo $product->separados($today);
										?>
										</td>
										<td>
											<form action="{{ Gate::check('delete_product')?action('ProductsController@destroy', [$product->id]):'' }}" method="post">
												<input name="_method" type="hidden" value="DELETE">
												{{ csrf_field() }}
												<a href="{{ action('ProductsController@edit', $product->id) }}" class="btn btn-xs btn-primary" data-toggle="tooltip" title="Editar Producto"><i class="fa fa-pencil" aria-hidden="true"></i></a>
												@can('delete_product')
												<button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Eliminar Producto"><i class="fa fa-times" aria-hidden="true"></i></button>
												@endcan
											</form>
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							@can('create_product')
							<span class="btn btn-danger" data-toggle="modal" data-target="#import-excel">
								Importar Excel
							</span>
							<a href="{{ action('ProductsController@create') }}" class="btn green"> Crear
								<i class="fa fa-plus"></i>
							</a>
							@endcan
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="import-excel" tabindex="-1" role="dialog" aria-labelledby="Importar">
	<div class="modal-dialog" role="document">
		<form action="{{ Gate::check('create_product')?action('ProductsController@import'):'' }}" class="form-horizontal" method="post" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="Importar">Importar Excel</h4>
				</div>
				<div class="modal-body">
					<input type="file" name="import_file" />
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-primary">Subir</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip(); 
});
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDgVWtTCbEbnag9SrhyE0cfZBaomCeqge8&callback=initMap" async defer></script>

@endsection