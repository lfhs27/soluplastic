@extends('layouts.dashboard')

@section('content')
<div class="page-content">
	<!-- BEGIN PAGE HEADER-->
	<h1 class="page-title"> {{ ucfirst($sidemenu) }}
		<!--small>subheader</small-->
	</h1>
	<div class="page-bar">
		<ul class="page-breadcrumb">
			<li>
				<i class="icon-home"></i>
				<a href="index.html">Home</a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<span>Ingresos</span>
			</li>
		</ul>
	</div>
	<!-- END PAGE HEADER-->
	@if(session('success'))
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				{{ session('success') }}
			</div>
		</div>
	</div>
	@endif
	@if(session('error'))
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				{{ session('error') }}
			</div>
		</div>
	</div>
	@endif
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="portlet light ">
				<div class="portlet-body">
					<form action="{{ action('ReportsController@entries') }}" method="get">
						<div class="row">
							<div class="col-md-6">
								<input type="text" name="q" class="form-control" placeholder="Buscar por fecha o remisión" />
							</div>
							<div class="col-md-6">
								<input type="submit" class="btn btn-success" value="Buscar" />
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="portlet light ">
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-12">
							<table class="table table-hover table-light">
								<thead>
									<tr>
										<th>Fecha de Registro</th>
										<th>Fecha de Entrada</th>
										<th>Proveedor</th>
										<th>Producto</th>
										<th>Clasificación</th>
										<th>Medida</th>
										<th>Color</th>
										<th>Marca</th>
										<th>Cantidad</th>
										<th>Tipo</th>
										<th>Remisión</th>
										<th>Comentarios</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
								@foreach($ingresos as $ingreso)
									<tr>
										<td>{{ $ingreso->register_date }}</td>
										<td>{{ $ingreso->entry_date }}</td>
										<td>{{ $ingreso->provider }}</td>
										<td>
										@foreach($ingreso->products as $ingProd)
											{{ $ingProd->name }}<br />
										@endforeach
										</td>
										<td>
										@foreach($ingreso->products as $ingProd)
											{{ $ingProd->classification->name }}<br />
										@endforeach
										</td>
										<td>
										@foreach($ingreso->products as $ingProd)
											{{ $ingProd->width }}" x {{ $ingProd->height }}" x {{ $ingProd->length }}"<br />
										@endforeach
										</td>
										<td>
										@foreach($ingreso->products as $ingProd)
											{{ $ingProd->color->name }}<br />
										@endforeach
										</td>
										<td>
										@foreach($ingreso->products as $ingProd)
											{{ $ingProd->brand->name }}<br />
										@endforeach
										</td>
										<td>
										@foreach($ingreso->products as $ingProd)
										{{ 
										$ingProd->pivot->quantity_verde + 
										$ingProd->pivot->quantity_azul + 
										$ingProd->pivot->quantity_amarillo + 
										$ingProd->pivot->quantity_naranja + 
										$ingProd->pivot->quantity_rojo + 
										$ingProd->pivot->devueltos
										}}
										<br />
										@endforeach
										</td>
										<td>{{ ucfirst($ingreso->type) }}</td>
										<td>{{ $ingreso->remision }}</td>
										<td>{{ $ingreso->comentarios }}</td>
										<td></td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip({html: true}); 
});

</script>

@endsection