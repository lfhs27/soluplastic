<div class="page-sidebar-wrapper">
	<!-- END SIDEBAR -->
	<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
	<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
	<div class="page-sidebar navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
		<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
		<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
		<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
			<li class="nav-item start {{ (isset($sidemenu) && strcmp($sidemenu, 'dashboard') == 0) ? 'active open': '' }}">
				<a href="{{ action('DashboardController@index') }}" class="nav-link nav-toggle">
					<i class="fa fa-dashboard"></i>
					<span class="title">Dashboard</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
			</li>
			<li class="nav-item {{ (isset($sidemenu) && strcmp($sidemenu, 'productos') == 0) ? 'active open': '' }}">
				<a href="{{ action('ProductsController@index') }}" class="nav-link nav-toggle">
					<i class="fa fa-archive"></i>
					<span class="title">Productos</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
			</li>
			@can('crud_config')
			<li class="nav-item {{ (isset($sidemenu) && strcmp($sidemenu, 'colores') == 0) ? 'active open': '' }}">
				<a href="javascript:;" class="nav-link nav-toggle">
					<i class="fa fa-cog"></i>
					<span class="title">Configuración</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				
				<ul class="sub-menu">
					<li class="nav-item">
						<a href="{{ action('ProductTypesController@index') }}" class="nav-link ">
							<span class="title">Tipos de Producto</span>
							<span class="badge"><i class="fa fa-bars"></i></span>
						</a>
					</li>
					<li class="nav-item">
						<a href="{{ action('ColorsController@index') }}" class="nav-link ">
							<span class="title">Colores</span>
							<span class="badge"><i class="fa fa-tint"></i></span>
						</a>
					</li>
					<li class="nav-item">
						<a href="{{ action('ClassificationsController@index') }}" class="nav-link ">
							<span class="title">Clasificaciones</span>
							<span class="badge"><i class="fa fa-tint"></i></span>
						</a>
					</li>
					<li class="nav-item">
						<a href="{{ action('CategoriesController@index') }}" class="nav-link ">
							<span class="title">Categorías</span>
							<span class="badge"><i class="fa fa-tint"></i></span>
						</a>
					</li>
					<li class="nav-item">
						<a href="{{ action('BasesController@index') }}" class="nav-link ">
							<span class="title">Bases</span>
							<span class="badge"><i class="fa fa-window-minimize"></i></span>
						</a>
					</li>
					<li class="nav-item">
						<a href="{{ action('DoorsController@index') }}" class="nav-link ">
							<span class="title">Puertas</span>
							<span class="badge"><i class="fa fa-tint"></i></span>
						</a>
					</li>
					<li class="nav-item">
						<a href="{{ action('BrandsController@index') }}" class="nav-link ">
							<span class="title">Marcas</span>
							<span class="badge"><i class="fa fa-tint"></i></span>
						</a>
					</li>
				</ul>
			</li>
			@endcan
			<li class="nav-item {{ (isset($sidemenu) && strcmp($sidemenu, 'ordenes') == 0) ? 'active open': '' }}">
				<a href="javascript:;" class="nav-link nav-toggle">
					<i class="fa fa-file-o"></i>
					<span class="title">Ordenes</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">
					@can('create_orders')
					<li class="nav-item">
						<a href="{{ action('OrdersController@create') }}" class="nav-link ">
							<span class="title">Nueva Orden</span>
							<span class="badge"><i class="fa fa-plus"></i></span>
						</a>
					</li>
					 @endcan
					<li class="nav-item">
						<a href="{{ action('OrdersController@index') }}" class="nav-link ">
							<span class="title">Abiertas</span>
							<span class="badge"><i class="fa fa-bars"></i></span>
						</a>
					</li>
					<li class="nav-item">
						<a href="{{ action('OrdersController@liberadas') }}" class="nav-link ">
							<span class="title">Liberadas</span>
							<span class="badge"><i class="fa fa-bars"></i></span>
						</a>
					</li>
				</ul>
			</li>
			<li class="nav-item {{ (isset($sidemenu) && strcmp($sidemenu, 'ingresos') == 0) ? 'active open': '' }}">
				<a href="{{ action('IngresosController@index') }}" class="nav-link nav-toggle">
					<i class="fa fa-flip-horizontal fa-truck"></i>
					<span class="title">Ingresos</span>
					<span class="arrow"></span>
				</a>
			</li>
			@can('crud_users')
			<li class="nav-item {{ (isset($sidemenu) && strcmp($sidemenu, 'usuarios') == 0) ? 'active open': '' }}">
				<a href="{{ action('UsersController@index') }}" class="nav-link nav-toggle">
					<i class="icon-users"></i>
					<span class="title">Usuarios</span>
					<span class="arrow"></span>
				</a>
			</li>
			@endcan
			<li class="nav-item {{ (isset($sidemenu) && strcmp($sidemenu, 'reportes') == 0) ? 'active open': '' }}">
				<a href="javascript:;" class="nav-link nav-toggle">
					<i class="fa fa-line-chart"></i>
					<span class="title">Reportes</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				
				<ul class="sub-menu">
					<li class="nav-item">
						<a href="{{ action('ReportsController@entries') }}" class="nav-link ">
							<span class="title">Ingresos</span>
							<span class="badge"><i class="fa fa-truck"></i></span>
						</a>
					</li>
				</ul>
			</li>
		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
	<!-- END SIDEBAR -->
</div>