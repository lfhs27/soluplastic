@extends('layouts.dashboard')

@section('content')
<div class="page-content">
	<!-- BEGIN PAGE HEADER-->
	<h1 class="page-title"> {{ ucfirst($sidemenu) }}
		<!--small>subheader</small-->
	</h1>
	<div class="page-bar">
		<ul class="page-breadcrumb">
			<li>
				<i class="icon-home"></i>
				<a href="index.html">Home</a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<span>Dashboard</span>
			</li>
		</ul>
	</div>
	<!-- END PAGE HEADER-->
	<div class="row filtros">	
		<div class="col-md-6 col-sm-12">
			<div class="portlet light ">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-bubble font-dark hide"></i>
						<span class="caption-subject font-hide bold uppercase">Interéses</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-12">
							<label><input type="checkbox" name="interes" value="seguridad" /> Seguridad</label>
							<label><input type="checkbox" name="interes" value="salud" /> Salud</label>
							<label><input type="checkbox" name="interes" value="genero" /> Genero</label>
							<label><input type="checkbox" name="interes" value="movilidad" /> Movilidad</label>
							<label><input type="checkbox" name="interes" value="participacion" /> Participación</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-sm-12">
			<div class="portlet light ">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-bubble font-dark hide"></i>
						<span class="caption-subject font-hide bold uppercase">Género</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-12">
							<label><input type="checkbox" name="genero" value="hombre" checked /> Hombre</label>
							<label><input type="checkbox" name="genero" value="mujer" checked /> Mujer</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8 col-sm-12">
			<div class="portlet light ">
				<!--div class="portlet-title">
					<div class="caption">
						<i class="icon-bubble font-dark hide"></i>
						<span class="caption-subject font-hide bold uppercase">Test</span>
					</div>
					<div class="actions">
						<div class="btn-group">
							<a class="btn green-haze btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
								<i class="fa fa-angle-down"></i>
							</a>
							<ul class="dropdown-menu pull-right">
								<li>
									<a href="javascript:;"> Option 1</a>
								</li>
								<li class="divider"> </li>
								<li>
									<a href="javascript:;">Option 2</a>
								</li>
								<li>
									<a href="javascript:;">Option 3</a>
								</li>
								<li>
									<a href="javascript:;">Option 4</a>
								</li>
							</ul>
						</div>
					</div>
				</div-->
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-12">
							<div id="map" style="height:600px;"></div>
							<div id="capture"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-md-4 col-sm-12">
			<div class="portlet light ">
				<div class="portlet-body tableCluster">
					<div class="row">
						<div class="col-md-12">
							<table id="tableCluster" class="table">
								<thead>
									<tr>
										<th>Nombre</th>
										<th>Sexo</th>
										<th>Interés</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

    <script>
      var map;
      var markerCluster;
      var markersMaster = [];
      var filteredMarkers = [];
	  var filters = [];
	  lines = [];
	  points = [];
	$(document).ready(function() {
		$(".filtros input").change(function(e){
			filters = [];
			$('.filtros input:checked').each(function() {
				filters.push($(this).val());
			});
			filter(filters);
		});
	});

function processData() {
	$.ajax({
		type: "GET",
		url: "/csv/prospectos.csv",
		dataType: "text",
		success: function(data) {
			allText = data;
			
			var allTextLines = allText.split("\n");
			
			allTextLines.shift();
			$.each(allTextLines, function(i, val){
				var line = val.split(";");
				var point = {
					lat : parseFloat(line[3]),
					lng : parseFloat(line[4])
				};
				if(line[0].trim() != ""){
					points.push(point);
					lines.push(line);
				}
			});
			
			initMap();
		}
	 });
}
function initMap() {

	map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          center: {lat: 25.6866142, lng: -100.3161126}
        });

        // Create an array of alphabetical characters used to label the markers.
        var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // Add some markers to the map.
        // Note: The code uses the JavaScript Array.prototype.map() method to
        // create an array of markers based on a given "locations" array.
        // The map() method here has nothing to do with the Google Maps API.		
        markersMaster = lines.map(function(line, i) {
			var location = {
					lat : parseFloat(line[3]),
					lng : parseFloat(line[4])
				};
			var infowindow = new google.maps.InfoWindow({
				content: '<strong>Nombre:</strong> '+line[0]+'<br /><strong>Sexo:</strong> '+line[1]+'<br /><strong>Interés:</strong> '+line[2]
			});
			var marker = new google.maps.Marker({ 
				position: location,
				lineData : line,
				label: labels[i % labels.length]
			});
			marker.addListener('click', function() {
				infowindow.open(map, marker);
			});
			
			return marker;
        });

    // Add a marker clusterer to manage the markers.
	markerCluster = new MarkerClusterer(
								map, 
								markersMaster, 
								{
									imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
								}
							);
	google.maps.event.addListener(markerCluster, "clusterclick", function (cluster) {
		clusterMarkers = cluster.getMarkers();
		html = '';
		$.each(clusterMarkers, function(i, val){
			html += '<tr>';
			html += '	<td>'+val.lineData[0]+'</td>';
			html += '	<td>'+val.lineData[1]+'</td>';
			html += '	<td>'+val.lineData[2]+'</td>';
			html += '</tr>';
		});
		$("#tableCluster tbody").html(html);
	}); 
}
function filter(){
	filteredMarkers = [];
	for(var i = 0; i < markersMaster.length; i++){
		markersMaster[i].setMap(null);
		if($.inArray(markersMaster[i].lineData[2], filters) > -1 && $.inArray(markersMaster[i].lineData[1], filters) > -1){
			filteredMarkers.push(markersMaster[i]);
		}
	}
	for(var i = 0; i < filteredMarkers.length; i++){
		filteredMarkers[i].setMap(map);
	}
	markerCluster.clearMarkers();
	markerCluster.addMarkers(filteredMarkers);
	
}
    </script>
	<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDgVWtTCbEbnag9SrhyE0cfZBaomCeqge8&callback=processData">
    </script>

@endsection