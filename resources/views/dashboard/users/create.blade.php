@extends('layouts.dashboard')

@section('content')
<div class="page-content">
	<!-- BEGIN PAGE HEADER-->
	<h1 class="page-title">
		Nuevo Usuario
		<!--small>subheader</small-->
	</h1>
	<!-- END PAGE HEADER-->
	<form action="{{ action('UsersController@store') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<div class="portlet light">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-bubble font-dark hide"></i>
							<span class="caption-subject font-hide bold uppercase">Información del Usuario</span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Nombre</label>
								<div class="col-sm-8">
									<input type="text" name="name" class="form-control" id="" value="">
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">E-mail</label>
								<div class="col-sm-8">
									<input type="text" name="email" class="form-control" id="" value="">
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Password</label>
								<div class="col-sm-8">
									<input type="password" name="password" class="form-control" id="" value="">
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Rol</label>
								<div class="col-sm-8">
									@foreach($roles as $role)
									<label><input type="checkbox" name="roles[]" class="" id="" value="{{ $role->id }}"> {{ $role->name }} </label>
									@endforeach
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<input type="submit" class="btn btn-success" value="Guardar" style="width:100%;" />
			</div>
		</div>
	</form>
</div>

<script>
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip(); 
});
</script>

@endsection