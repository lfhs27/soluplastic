<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Route::group(['middleware' => 'auth'], function () {
	Route::get('/dashboard', 'DashboardController@index');

	Route::get('/orders/liberadas', 'OrdersController@liberadas');
	Route::post('/orders/massive', 'OrdersController@massive');
	
	Route::post('/productos/import', 'ProductsController@import');
	
	Route::resource('/productos', 'ProductsController');
	Route::resource('/colors', 'ColorsController');
	Route::resource('/brands', 'BrandsController');
	Route::resource('/product-types', 'ProductTypesController');
	Route::resource('/classifications', 'ClassificationsController');
	Route::resource('/categories', 'CategoriesController');
	Route::resource('/bases', 'BasesController');
	Route::resource('/doors', 'DoorsController');
	Route::resource('/orders', 'OrdersController');
	Route::resource('/entries', 'IngresosController');
	Route::resource('/users', 'UsersController');

	Route::get('/orders/{id}/liberar', 'OrdersController@liberar');
	Route::get('/orders/{id}/separar', 'OrdersController@separar');
	Route::post('/orders/estimate', 'OrdersController@estimate');
	
	Route::get('/sendmail', 'OrdersController@sendmail');
	
	Route::prefix('reports')->group(function () {
		Route::get('/entries', 'ReportsController@entries');
	});
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
